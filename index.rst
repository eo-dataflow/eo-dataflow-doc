.. -*- coding: utf-8 -*-

====================
 eo-dataflow-manager
====================

**eo-dataflow-manager** is a free software solution, written in python, whose goal is to provide producers and users of Earth observation data with an open-source, flexible and reusable tool to allow the downloading of data according to their availability.


Credits
=======
.. image:: _static/copernicus.jpg
  :width: 300

**eo-dataflow-manager** is developed by Ifremer_ and supported by Copernicus_.

.. _Ifremer: https://www.ifremer.fr
.. _Copernicus: https://www.copernicus.eu

**eo-dataflow-manager** was initially founded and supported by the `European Space Agency
(ESA)`_, Eumetsat_.

.. _European Space Agency (ESA): https://esa.int
.. _Eumetsat: https://eumetsat.int


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: For users

   Overview
   Installation
   Configuration
   Configuring_a_dataset_download
   Configuring_report
   
.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: For developers/contributors

   Operation_and_maintenance
   Functional_diagrams
   
License¶
========
**felyx** is available under the open source `GPLv3 license`__.

__ https://www.gnu.org/licenses/gpl-3.0.en.html
