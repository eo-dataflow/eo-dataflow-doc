.. -*- coding: utf-8 -*-

Configuring reports and alerts
==============================

Overview
--------

This section describes how to launch the eo-dataflow report and alert applications as well as the configuration of these applications.

This configuration includes all the modifiable options of the application.

Application for eo-dataflow report and dashboards generation
-------------------------------------------------------------

Produce monitoring reports from eo-dataflow data and generate Grafana dashboards and alerts.

The application has two endpoints : eo-dataflow-index-reports and eo-dataflow-create-dashboards.


eo-dataflow-index-reports
~~~~~~~~~~~~~~~~~~~~~~~~~~

The *eo-dataflow-index-reports* collects downloaded eo files metrics and index them into elasticsearch indices.
The input is a configuration file.

::

  usage: eo-dataflow-index-reports [-h] [-c CONFIGURATION_FILE] [--logfile LOGFILE]
                                   [--logfile_level LOGFILE_LEVEL] [-v | -q | -s]
                                   [-P [key=value [key=value ...]]]
                                   [-C [key=value [key=value ...]]]

  optional arguments:
    -h, --help         show this help message and exit
    -c CONFIGURATION_FILE, --configuration_file CONFIGURATION_FILE
                       Configuration file path
    --logfile LOGFILE  Path of the file where logs will be written
    --logfile_level LOGFILE_LEVEL
                       Minimal level that log messages must reach to be written in the log file
    -v, --verbose      Activate debug level logging - for extra feedback.
    -q, --quiet        Disable information logging - for reduced feedback.
    -s, --silence      Log ONLY the most critical or fatal errors.
    -P [key=value [key=value ...]]
                       Update of parameter items
    -C [key=value [key=value ...]]
                       Update of configuration items


eo-dataflow-create-reports
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The *eo-dataflow-create-dashboards* creates dashboards from metrics indexed into Elasticsearch.
The input is a configuration file.

::

  usage: eo-dataflow-create-dashboards [-h] [-c CONFIGURATION_FILE] [--logfile LOGFILE]
                                       [--logfile_level LOGFILE_LEVEL] [-v | -q | -s]
                                       [-P [key=value [key=value ...]]]
                                       [-C [key=value [key=value ...]]]


  optional arguments:
    -h, --help         show this help message and exit
    -c CONFIGURATION_FILE, --configuration_file CONFIGURATION_FILE
                       Configuration file path
    --logfile LOGFILE  Path of the file where logs will be written
    --logfile_level LOGFILE_LEVEL
                       Minimal level that log messages must reach to be written in the log file
    -v, --verbose      Activate debug level logging - for extra feedback.
    -q, --quiet        Disable information logging - for reduced feedback.
    -s, --silence      Log ONLY the most critical or fatal errors.
    -P [key=value [key=value ...]]
                       Update of parameter items
    -C [key=value [key=value ...]]
                       Update of configuration items

Default eo-dataflow report and alert configuration
--------------------------------------------------

The default configuration of the eo-dataflow-manager is shown below This section describes how to configure the reports.

.. code-block:: yaml

    #---------------------------------------
    # EO dataflow report configuration file
    #---------------------------------------

    # eo-dataflow-index-reports
    #--------------------------
    # list of folders to be harvested
    input_paths:
      - '/data/eo_dataflow/workspace/spools/monitoring'

    # workspace root directory
    workspace_path: '/data/eo_dataflow/workspace'

    # Elastic search configuration
    elasticsearch:
      # URL of the ES server, credentials and port
      url: 'http://127.0.0.1:9200'
      # Prefix for the ES indexes
      prefix: 'eo_dataflow_report'
      # Default configuration for ES operations
      # Size of data returned by search request
      chunksize: 5000
      # Timeout in seconds for a search request
      timeout: 10s
      # Delay of retention in minutes after a scroll request
      scroll_retention_time: 5m
      # Configuration for index operation (used for registering reports)
      operations:
        report_index:
          chunksize: 500
          timeout: 180s

    # eo-dataflow-index-reports
    #--------------------------
    # Grafana configuration

    grafana:
      root_folder: 'eo_dataflow_report'
      host: '127.0.0.1:3000'
      api_key: 'eyJrIjoiZExGN0U3dTlBeTg2SFJMN3E1QmZUcTFRTXNVNHFFU3ciLCJuIjoiYWRtaW4iLCJpZCI6MX0='
      ssl_verify: False
      search:
        api_endpoint: '/api/search?query=%'
        dashboard_type: 'dash-db'
      library_element:
        api_end_point: '/api/library-elements'
      alert_rules:
        api_endpoint: '/api/ruler/grafana/api/v1/rules/'
        alert_evaluation_period: '1m'
        alert_firing_delay: '5m'
        alert_relative_timerange_start: 864000
        alert_relative_timerange_end: 0
      notifications:
        api_endpoint: '/api/alertmanager/grafana/config/api/v1/alerts'
      datasources:
        type: 'elasticsearch'
        access: 'proxy'
        url: 'https://127.0.0.1:9200' # TODO : merge common keys with elasticsearch credentials ?
        user: ""
        password: ""
        basic_auth: True
        time_field_name: 'sensing_time'
        json_data:
          es_version: '7.0.0'
          include_frozen: false
          max_concurrent_shard_requests: 5
          time_interval: '1d'
      dashboards:
        plugin_version: '8.4.2'
        time_range_from_days: 90
        time_range_to_days: 0
        groups:
          - name: 'test_un'
            downloads:
              - name: 'SST_noaa_avrrr_only'
                monitoring:
                  alert_threshold_nb_of_files_downloaded_per_sensing_day: 14  # TBC
                  alert_threshold_nb_of_files_downloaded_per_day: 1  # TBC
          - name: 'test_deux'
            downloads:
              - name : 'navy_nrlmry_2022'
                monitoring:
                  alert_threshold_nb_of_files_downloaded_per_sensing_day: 1  # TBC
                  alert_threshold_nb_of_files_downloaded_per_day: 1  # TBC
              - name: 'odyssea_abi_goes17'
                monitoring:
                  alert_threshold_nb_of_files_downloaded_per_sensing_day: 500  # TBC
                  alert_threshold_nb_of_files_downloaded_per_day: 1  # TBC

    contact_points:
      - name: 'name1'
        addresses:
          - 'your.address@domain.com'
          - 'your.other.address@domain.com'
        disable_resolve_message: False
        repeat_interval:
          duration: 12
          unit: 'h'
      - name: 'name2'
        addresses:
          - 'your.address@domain.com'
          - 'your.other.address@domain.com'
        disable_resolve_message: False
        repeat_interval:
          duration: 12
          unit: 'h'


Configuration details
---------------------

Input paths configuration
~~~~~~~~~~~~~~~~~~~~~~~~~

The following list contains the folders to be harvested.

Example:

.. code-block:: yaml

  input_paths:
    - 'tests/resources/dist_dirs/dir1/metrics'
    - 'tests/resources/dist_dirs/dir2/metrics'

Workspace configuration
~~~~~~~~~~~~~~~~~~~~~~~

The following entry specifies the workspace root directory.

Example:

.. code-block:: yaml

  workspace_path: 'tests/resources/workspace'


Dashboards configuration
~~~~~~~~~~~~~~~~~~~~~~~~

Set the dashboards' time range using the following options :

  * *grafana.dashboards.time_range_from_days*, set to now -90 days by default
  * *grafana.dashboards.time_range_to_days*, set to now by default

Example :

.. code-block:: yaml

  grafana:
    dashboards:
      plugin_version: '8.4.2'
      time_range_from_days: 90
      time_range_to_days: 0


Downloads configuration
~~~~~~~~~~~~~~~~~~~~~~~~
Configure downloads and their monitoring options in the `grafana.dashboards.groups` section.

Example :

.. code-block:: yaml

    groups:
      - name: 'cwwic cfosat'
        downloads:
          - name: 'cwwic_cfosat_swisca_l2s'
          - name: 'cwwic_cfosat_swi_l1a'
      - name: 'sentinel'
        downloads:
          - name : 'esa_s1b_wv_ocn'
          - name: 'esa_s1a_wv_ocn'

1 dashboard per group under the root folder and 4 panels by download are created:

 * number of files downloaded per sensing day
 * number of files downloaded per day
 * average delay between sensing time and download time, per sensing day
 * maximum delay between the file modification time and the download time, per day

Alerts configuration
~~~~~~~~~~~~~~~~~~~~

Two optional alerts can be configured by adding a *monitoring* section on the following panels :

 * number of files downloaded per sensing day: This alert triggers when the number of files downloaded per sensing day is below the value defined in `alert_threshold_nb_of_files_downloaded_per_sensing_day`

 * number of files downloaded per day: This alert triggers when the number of files downloaded per day is below the value defined in `alert_threshold_nb_of_files_downloaded_per_day`

Example :

.. code-block:: yaml

  - name: 'cwwic cfosat'
    downloads:
      - name: 'cwwic_cfosat_swisca_l2s'
        monitoring:
          alert_threshold_nb_of_files_downloaded_per_sensing_day: 14
          alert_threshold_nb_of_files_downloaded_per_day: 1
      - name: 'cwwic_cfosat_swi_l1a'
        monitoring:
          alert_threshold_nb_of_files_downloaded_per_sensing_day: 17
          alert_threshold_nb_of_files_downloaded_per_day: 1
  - name: 'sentinel'
    downloads:
      - name : 'esa_s1b_wv_ocn'
        monitoring:
          alert_threshold_nb_of_files_downloaded_per_sensing_day:
          alert_threshold_nb_of_files_downloaded_per_day: 1
      - name: 'esa_s1a_wv_ocn'
        monitoring:
          alert_threshold_nb_of_files_downloaded_per_sensing_day: 500
          alert_threshold_nb_of_files_downloaded_per_day: 1


Set the `alert_threshold_<name>` value to -1 to disable an alert.

The alerts are displayed in the Grafana UI in the `Alert rules` page under the defined `root_folder`.

In addition to the Grafana native filters (data source, State, Rule type...), alerts can also be filtered by labels :

 * application_name
 * group_name
 * download_name
 * dashboard_uid

Example of query to enter in `Search by label`:

.. code-block:: yaml

  {application_name="eo_dataflow_report", group_name="cwwic cfosat"}


Alert notifications configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Configure optional `contact_points` section to receive mail notifications on alerts.

By default the repeat interval is set to 12 hours and the `disable_resolve_message` option is set to False.

.. code-block:: yaml

  contact_points:
  - name: 'name1'
    addresses:
      - 'your.address@domain.com'
      - 'your.other.address@domain.com'
    disable_resolve_message: False
    repeat_interval:
      duration: 12
      unit: 'h'

  - name: 'name2'
    addresses:
      - 'your.address@domain.com'
      - 'your.other.address@domain.com'
    disable_resolve_message: False
    repeat_interval:
      duration: 12
      unit: 'h'

This will create `Contact points` and `Notification policies` displayed in the Grafana UI `Alerting` page.


Configuration file documentation
--------------------------------

This section describes each entry of the configuration file.

It must be in YAML format and contains the following sections :

 * General index configuration
 * Elasticsearch configuration
 * General reports configuration
 * Grafana configuration
 * Alerting contact points configuration

General index configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section defines the Elasticsearch server access and operations configuration.

**input_paths:**
    List of folder paths to harvest. Example ‘/eo-dataflow/workspace/spool/reporting’

**workspace_path:**
    workspace root directory path. Example’tests/workspace’


Elasticsearch configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This section defines the Elasticsearch server access and operations configuration.

**elasticsearch:**
    **url:**
        URL of the ES server, credentials and port. Example: https://user:password@host:9200

    **prefix:**
        Prefix for the Elasticsearch indices. Example: `eo_dataflow_report_`

    **Default configuration for ES operations**

    The following entries are defined for default Elasticsearch operations.

    **chunksize:**
        Size of data returned by requests (in bytes). Example : 5000.

    **timeout:**
        Timeout in seconds for requests (in seconds). Example : 10s.

    **scroll_retention_time:**
        Delay of retention in minutes after a scroll request. Example : 5m.

    **operations:**
        **report_index:**
            Configuration for index operation (used for registering reports).

            **chunksize:**
                Size of data returned by indexing requests (in bytes).

            **timeout:**
                Timeout in seconds for indexing requests (in seconds).

General reports configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**root_folder:**
    Dashboard root folder name. Example : eo-dataflow


Grafana configuration
~~~~~~~~~~~~~~~~~~~~~

This section defines the Grafana server access and dashboards and alert configuration.

**grafana:**

  **root_folder:**
    Dashboards' root folder

  **host:**
    URL of the Grafana server host. Example:  'grafana.domain.country'.

  **api_key:**
    Grafana API key string (This key must be previously generated with the Grafana UI).

  **ssl_verify:**
    Enables the SSL verify option for HTTPS protocol. Protocol HTTPS is used if set to True, protocol HTTP is used otherwise.

  **search:**
    The following entries define the Grafana search configuration.

    **api_endpoint:**
        The Grafana search API endpoint. Example : /api/search?query=%.

    **dashboard_type:**
        The Grafana dashboard type. Set to : 'dash-db'.

  **library_element:**
    **api_end_point:**
        The Grafana library elements API endpoint. Example: '/api/library-elements'

  **alert_rules:**
    The following entries define the alert rules configuration.

    **api_endpoint:**
        The Grafana alert API endpoint. Example : /api/ruler/grafana/api/v1/rules/

    **alert_evaluation_period:**
        Specify how often Grafana should evaluate the alert rules.
        Example : 1m means the evaluation period is set to 1 minute.

    **alert_firing_delay:**
        Once condition is breached, alert will go into pending state. If it is pending for longer than the "for" value, it will become a firing alert. Example: 5m means a pending alert will turn to firing state after 5 minutes

    **alert_relative_timerange_start:**
        Specify the time range start for alert evaluation. The value is expressed in seconds relative from now.
        Example : 86400 means the time range start is set to one day before now.

    **alert_relative_timerange_end:**
        Specify the time range end for alert evaluation. The value is expressed in seconds relative from now.
        Example : 0 means the time range end is set to now.

  **notifications:**
    The following entries define the alert notification configuration.

    **api_endpoint:**
        The Grafana notification API endpoint. Example /api/alertmanager/grafana/config/api/v1/alerts

  **datasources:**
    The following entries define the Grafana datasources configuration.

    **type:**
        The type of the datasource. Set to : elasticsearch.

    **access:**
        The Grafana datasources access mode, proxy or direct (Server or Browser in the UI). Set to : proxy.

    **url:**
        The URL of the Elasticsearch server. Example:  https://host:9200.

    **user:**
        The Elasticsearch user.

    **password:**
        The Elasticsearch password

    **basic_auth:**
        Enable or disable authentication using Basic Auth, with the given user and password above. Set to : True.

    **time_field_name:**
        The name of Elasticsearch time field used by Grafana. Set to : field sensing_time.

    **json_data:**
      **es_version:**
        The Elasticsearch version. Example: '7.0.0'.

      **max_concurrent_shard_requests:**
        Maximum number of concurrent shard requests that each sub-search request executes per node. Set to: 5.

      **include_frozen:**
        If set to True, Grafana will not ignore frozen indices when performing search requests. Set to: false.

      **time_interval:**
        A lower limit for the auto group by time interval. Recommended to be set to write frequency. Set to: '1d'.

  **notifications:**
    The following entries define the alert notification configuration.

    **api_endpoint:**
        The Grafana notification API endpoint. Example /api/alertmanager/grafana/config/api/v1/alerts

  **dashboards:**
    The following entries define the dashboards configuration.

    **plugin_version:**
        The Grafana plugin version. Example: '8.4.2'.

    **time_range_from_days:**
        Specify the number of days from today for the time range start of the dashboards. Example : 10.

    **time_range_to_days:**
        Specify the number of days from today for the time range end of the dashboards. Example : 1.
        A value of 0 means the time range end is set to today.

    **groups:**
        This section specifies the download groups.

        **name:**
            The name of the group of downloads. Example: 'cwwic cfosat'.

            **downloads:**
                **name:**
                    The name of the download. Example: 'cwwic_cfosat_swisca_l2s'.

                    **monitoring:**
                        **alert_threshold_nb_of_files_downloaded_per_sensing_day:**
                            | The number of downloaded files per sensing day threshold for alerting.
                            | Example : 14.
                        **alert_threshold_nb_of_files_downloaded_per_day:**
                            | The number of downloaded files per day threshold for alerting.
                            | Example : 1.

            ...

  **root_folder:**
    The name of the folder which will contain the Grafana dashboards. Example : eo_dataflow_report_test.

Contact_points configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If alerts has been generated, Grafana sends periodically alert notification emails to users.
This notifications can be configured in this section.

**contact_points:**
    **name:**
        Name of the contact. Example: name1.
    **addresses:**
      Email address of the contact. Example: your.address@domain.com

      Other email address of the contact. Example: your.other.address@domain.com


      **disable_resolve_message:**
        Option allows to disable the resolve message that is sent when the alerting state returns to false. Example: False

      **repeat_interval:**
        **duration:**
            | The waiting time to resend an alert after they have successfully been sent.
            | Example : 12
        **unit:**
            The unit of the waiting time. Example : 'h'

