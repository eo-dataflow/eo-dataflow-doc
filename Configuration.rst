.. -*- coding: utf-8 -*-

Configuring the manager
=======================

Overview
--------


The eo-dataflow-manager configuration contains the application settings and allows the
down eo-dataflow-manager loader options to be activated or deactivated.

This configuration includes all the modifiable options of the application.

Default eo-dataflow-manager configuration
-----------------------------------------


The default configuration of the eo-dataflow-manager is shown below

.. code-block:: yaml

  general:
    mainloop_timeout: 30
    max_activated_flow: 25
    max_activated_files_by_loop: 100
    ui_worker: false
    paths:
      workspace: /tmp/eo-dataflow-manager/workspace
      appdata: /srv/eo-dataflow-manager/
      
    admins: []
    
    emm:
      project: CERSAT
      message_type: download
      targets: []
      filesystem:
        path: spools/message/
      elasticsearch:
        hosts: [localhost]
        scheme: http
        user: null
        password: null
        index: dl-emm
        sniff_cluster: False
      rabbitmq:
        host: localhost
        port: 5672*
        ssl: false
        user: null
        password: null
        virtual_host: "/"
        queue_name: dl-emm
        routing_key: null*
        
    jobs:
      targets: []
        elasticsearch:
        hosts: []
        scheme: http
        user: null
        password: null
        index: dl-jobs
        sniff_cluster: False
      rabbitmq:
        host: localhost
        port: 5672
        ssl: false
        user: null
        password: null
        virtual_host: "/"
        queue_name: dl-jobsif
        routing_key: null
        
    metrics:
      targets: [filesystem]
      filesystem:
        path: spools/monitoring/
      rabbitmq:
        host: localhost
        port: 5672
        ssl: false
        user: null
        password: null
        virtual_host: "/"
        queue_name: dl-metrics
        routing_key: null
      
    ui_worker:
      queue: #{queue_name: { queue_arguments: {alias : alias_downloader_name}}}
        name: UI.host.0
        alias: alias_downloader_name
      broker_url: #"amqp://user:password@host:port/virtual_host"
        host: localhost
        port: 5672
        user: null*
        password: null
        virtual_host: "/"
        broker_heartbeat: 120
        imports: [eo_dataflow_manager.worker.celery_tasks]
        result_backend: amqp
        esult_expires: 300
        task_serializer: json
        result_serializer: json*
        timezone: Europe/Paris
        enable_utc: True
        result_compression: bzip2
        accept_content: [json]
      
    logs:
      default:
        root:
          handlers: [stdout]
          level: INFO
        loggers:
          elasticsearch: {level: WARNING}
          pika: {level: WARNING}
        handlers:
          stdout:
            class: logging.StreamHandler
            formatter: stdout_fmt
            stream: "ext://sys.stdout"
            filters: []  # info_operator
        formatters:
          stdout_fmt:
            (): "eo_dataflow_manager.ifr_lib_modules.ifr_logging.IfrColorFormatter"
            fmt: "%(log_color)s%(levelname).3s%(reset)s|%(message_log_color)s%(message)s%(reset)s  %(thin)s%(filename)s:%(lineno)d@%(funcName)s()%(reset)s"
        filters:
          # --- Filter used to suppress INFO messages not containing OPERATOR tag
          info_operator:
            (): eo_dataflow_manager.ifr_lib_modules.ifr_logging.InfoOperatorFilter
      file_log:
        handlers:
          - file:
              class: eo_dataflow_manager.ifr_lib_modules.ifr_logging.IfrFileHandler
              formatter: file_fmt
              # filename: ""
              filters: [] # info_operator
        formatters:
          file_fmt:
              format: "%(asctime)s|%(levelname)-8s|%(process)-5d|%(message)-100s|"
        filters:
          # --- Filter used to suppress INFO messages not containing OPERATOR tag
          info_operator:
          class: eo_dataflow_manager.ifr_lib_modules.ifr_logging.InfoOperatorFilter


Configuration file .yaml
------------------------




The eo-dataflow-manager configuration file allows the modification of the default
configuration of the application.

This file is indicated by the "-c" option when launching the application.

All the information in the configuration file overrides the default values.



Example of configuration file:

.. code-block:: yaml

  # --- Misc config options
  general:
    # -- 
    max_activated_files_by_loop: 100  
    # --
    ui_worker: true
    
  paths: # --- Folder paths
    # --- Workspace folder path 
    workspace: /export/home1/pmaissiat/workspace_v6
    # --- App data and config folder
    appdata: /export/home1/pmaissiat/appdata_v6 
    
  emm:
  # --- List of output target for writing EMM logs. Available targets:
  # - filesystem: Store emm log files to a directory
  # - elasticsearch: Push emm messages to an elasticsearch database
  # - rabbitmq: Push emm messages to a RabbitMQ queue
  targets: [filesystem,rabbitmq]
  # --- Shared EMM logs
  filesystem:
    # --- Path, either absolute or relative to the workspace
    path: spools/message/
    
  rabbitmq: # --- RabbitMQ configuration
    host: emm.host.name
    user: emm_user
    password: emm_password
    virtual_host: emm-download
    
  jobs:
    targets: [rabbitmq]
    rabbitmq:
      host: emm.host.name
      user: jobs_user
      password: jobs_password
      virtual_host: jobs-download
      
    metrics:
      targets: []
      
    ui_worker: # --- Communication with UI
      # Construction of the broker's URL: amqp://user:password@host:port/virtual_host
      # queue name: must be unique, 
      # queue alias: name visible in the user interface 
      queue:
        name: UI.host.prod
        alias: download_prod_1
      broker_url:
        host: emm.host.name
        user: ui_user
        password: ui_password 
        virtual_host: ui-download

  logs:
    default:
      handlers:
        stdout:
          filters: [info_operator]
      filters:
        # --- Filter used to suppress INFO messages not containing OPERATOR tag
        #     only if log_level is INFO
        info_operator:
          (): eo_dataflow_manager.ifr_lib_modules.ifr_logging.InfoOperatorFilter


Final configuration
-------------------




The configuration that will be used by the eo-dataflow-manager is, in order of priority:

* launch options "-w" and "-a"

* the information contained in the configuration file indicated by the launch option "-c"

* the default configuration

Configuration details
---------------------


General
~~~~~~~


This node contains the basic parameters of the eo-dataflow-manager:

**general**:
  **mainloop_timeout**:
      Number of seconds wait between each cycle ( Default: 30 )
  **max_activated_flow**:
      | Maximum number of simultaneous downloads for a provider (server / protocol pair)
      | ( Default:** 25 )
  **max_activated_files_by_loop**:
      Maximum number of files that can be downloaded per download cycle ( Default:** 100 )
  **ui_worker**:
      Activation indicator for the worker dedicated to the user interface ( Default:** False )

  **paths**:

      Contains the two basic paths of th eo-dataflow-manager

      **workspace**:
          eo-dataflow-manager workspace directory path ( Default:** /tmp/eo-dataflow-manager/workspace
      **appdata**:
          | eo-dataflow-manager configuration directory path
          | ( Default:** /srv/eo-dataflow-manager/ )


Admins
~~~~~~


This node contains a list of administrator contact emails: not used.

Emm
~~~


This node contains the configuration of the Enhanced Messaging Module :

**emm**:
  **project**:
       Project name ( Default: CERSAT )
  **message_type**:
       Message type ( Default: download )
  **targets**:
       List of output target ( Default: [] )

  **filesystem**: File system configuration node: used if "filesystem" appears in the targets

      **path**:
          EMM file write path    ( Default: spools/message/ )

  **elasticsearch**: Elasticsearch configuration node: used if "elasticsearch" appears in the targets

      **hosts**:
        List of cluster hosts ( Default: [localhost] )
      **scheme**:
        Protocol ( Default: http )
      **user**:
        User name  ( Default: null )
      **password**:
        Password ( Default: null )
      **index**:
        Index name ( Default: dl-emm )
      **sniff_cluster**:
         Additional node detection flag ( Default: False )

  **rabbitmq**:  RabbitMQ configuration node: used if "rabbitmq" appears in the targets

       **host**:
          RabbitMQ server ( Default: localhost )
       **port**:
          Connection port  ( Default: 5672 )
       **ssl**:
          Ssl context flag ( Default: false )
       **user**:
          User name ( Default: null )
       **password**:
          Password ( Default: null )
       **virtual_host**:
          Virtual host name ( Default: «/» )
       **queue_name**:
          Queue name ( Default: dl-emm )
       **routing_key**:
          Routing key ( Default: null )


Jobs
~~~~


This node contains the configuration of job status messages:

**jobs**:
  **project**:
       Project name ( Default: CERSAT )
  **message_type**:
       Message type ( Default: download )
  **targets**:
       List of output target ( Default: [] )

  **elasticsearch**: Elasticsearch configuration node: used if "elasticsearch" appears in the targets

      **hosts**:
        List of cluster hosts ( Default: [] )
      **scheme**:
        Protocol ( Default: http )
      **user**:
        User name  ( Default: null )
      **password**:
        Password ( Default: null )
      **index**:
        Index name ( Default: dl-jobs )
      **sniff_cluster**:
         Additional node detection flag ( Default: False )

  **rabbitmq**:  RabbitMQ configuration node: used if "rabbitmq" appears in the targets

       **host**:
          RabbitMQ server ( Default: localhost )
       **port**:
          Connection port  ( Default: 5672 )
       **ssl**:
          Ssl context flag ( Default: false )
       **user**:
          User name ( Default: null )
       **password**:
          Password ( Default: null )
       **virtual_host**:
          Virtual host name ( Default: «/» )
       **queue_name**:
          Queue name ( Default: dl-jobsif )
       **routing_key**:
          Routing key ( Default: null )


UI_Worker
~~~~~~~~~


This node contains the celery worker configuration of the user interface

**ui_worker**:
  **queue**:  Describes queue informations

      **name**:

          | Name of RabbitMQ queue used to communicate with the user interface
          | (Default: UI.host.0 )

      **alias**:
          Downloader name in the UI ( Default: alias_downloader_name )

  **broker_url**: Node for broker url

      **host**:
          RabbitMQ server ( Default: localhost )
      **port**:
          Connection port ( Default: 5672 )
      **user**:
          User name ( Default: null )
      **password**:
          Password  ( Default: null )
      **virtual_host**:
          Virtual host name  ( Default: «/» )
      **broker_heartbeat**:
           ( Default: 120 )
      **imports**:
          | List of modules to import when the worker starts.
          | ( Default: [eo_dataflow_manager.worker.celery_tasks] )
      **result_backend**:
          Backend type to store task results  ( Default: amqp )
      **result_expires**:
          Waiting time before destruction of the result (in seconds)  ( Default: 300 )
      **task_serializer**:
          Data serialization method for task activation ( Default: json )
      **result_serializer**:
          Data serialization format for returning tasks ( Default: json )
      **timezone**:
          ime zone ( Default: Europe/Paris )
      **enable_utc**:
          UTC activation ( Default: True )
      **result_compression**:
          Data compression format result  ( Default: bzip2 )
      **accept_content** :
          List of content-types/serializers to allow.  ( Default: [json] )


Logs
~~~~


This node contains the general configuration of the eo-dataflow-manager's loggers

**logs**:

  **Default**: Default settings

      **root**:
          **handlers**:
              Default: [stdout]
          **level**:
              Default: INFO

      **loggers**: third-party package log settings

          **elasticsearch**:
              Default: {level: WARNING}
          **pika**:
              Default: {level: WARNING}

      **Handlers**:
          **stdout**:
              **class**:
                  Class ( Default: logging.StreamHandler)
              **stream**:
                  Streal URL ( Default: "ext://sys.stdout")
              **formatter**:
                  Define the formatting to apply ( Default: stdout_fmt )
              **filters** :
                  List of filters to apply ( Default: [] )
      **formatter**
          **stdout_fmt**
              **()**:
                  Default: "eo-dataflow-manager.ifr_lib_modules.ifr_logging.IfrColorFormatter"
              **fmt**:
                  | Default: "%(log_color)s%(levelname).3s%(reset)s|%(message_log_color)s%(message)s%(reset)s
                  | %(thin)s%(filename)s:%(lineno)d@%(funcName)s()%(reset)s"
      **filters**:
          **info_operator**:
              **()**:
                  Default: eo_dataflow_manager.ifr_lib_modules.ifr_logging.InfoOperatorFilter

  **file_log**: Log file settings

      **Handlers**: list of handler

          **- file**:
              **class**:
                  Class ( Default: eo-dataflow-manager.ifr_lib_modules.ifr_logging.IfrFileHandler)
              **formatter**:
                  Define the formatting to apply ( Default: file_fmt)
              **filters**
                  List of filters to apply ( Default: [] )

      **Formatters**:
          **file_fmt**:
              **format**:
                  | set the format string
                  | ( Default: "%(asctime)s|%(levelname)-8s|%(process)-5d|%(message)-100s|")
      **filters**:  # Filters used to suppress certain messages: each filter defines the filter class to call
          **info_operator**:
              Class
                ( Default: eo_dataflow_manager.ifr_lib_modules.ifr_logging.InfoOperatorFilter )

