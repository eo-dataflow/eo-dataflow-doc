.. -*- coding: utf-8 -*-

Overview
========


This document is the user guide for the eo-dataflow-manager application.

This update of the document corresponds to version 7.10 of the eo-dataflow-manager.



This user guide presents


 * Installation and configuration of the software

 * Configuring a download

 * Configuring reports

 * A guide for software maintenance

 * An appendix containing diagrams describing the operation of the eo-dataflow-manager

