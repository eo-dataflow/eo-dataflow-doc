.. -*- coding: utf-8 -*-

Configuring a dataset download
==============================


This section describes how to configure a dataset download.

Overview
--------


Each download is configured with a XML configuration file named as
<download_id>.download.xml, where download_id is an identifier for your dataset id.



**Use only alphanumeric characters and the '_' character as space.**



*Note: it is usually a good idea to use the provider’s dataset identifier for the*
download_id *field, as a consistent and unambiguous choice.*



To activate a download, this configuration file must be placed in the following folder
where it will be automatically detected, starting the download:




+----------------------+
| <appdata>/downloads/ |
+----------------------+





Building a configuration file
-----------------------------


A XML download file follows this template :

.. code-block::

  <download_config id='download_id'>
    <data_source>
      <location>
        <protocol>Ftp</protocol>
        <protocol_option>{'FollowLinks': 'true'}</protocol_option>
        <login>anonymous</login>
        <password>anonymous</password>
        <rootpath>/PDGS_SL_2_WCT____NR/</rootpath>
        <server>oda.eumetsat.int</server>
      </location>
      <date_extraction plugin="DirNameBasedDataReader">
        <regexp>S3A_SL_2_WCT____([0-9]{8}).*</regexp>
        <format>%Y%m%dT%H%M%S</format>
      </date_extraction>
      <selection>
         <update>new|modified</update>
        <date_folders>
          <pattern></pattern>
          <backlog_in_days></backlog_in_days>
          <max_date></max_date>
          <min_date></min_date>
        </date_folders>
        <directories>
          <ignore_newer_than>60</ignore_newer_than>
          <ignore_sensing_time_older_than>4</ignore_sensing_time_older_than>
          <ignore_regexp></ignore_regexp>
          <regexp></regexp>
        </directories>
        <files>
          <regexp/>
          <ignore_sensing_time_older_than>4</ignore_sensing_time_older_than>
          <ignore_regexp></ignore_regexp>
        </files>
        <opensearch>
          <area></area>
          <dataset></dataset>
          <request_format></request_format>
        </opensearch>
      </selection>
    </data_source>
    <data_destination>
      <location>/data/...</location>
      <organization>
        <subpath>%Y/%j</subpath>
        <type>datetree</type>
      </organization>
      <post-processing>
        <checksum>md5</checksum>
        <compression>none</compression>
        <archive\>
      </post-processing>
    </data_destination>
    <download_settings>
      <database>
        <path></path>
        <purge_scans_older_than></purge_scans_older_than>
        <purge_all_scans>true|false</purge_all_scans>
      </database>
      <cycle_length>60</cycle_length>
      <max_nb_of_files_downloaded_per_cycle>24</max_nb_of_files_downloaded_per_cycle>
      <nb_parallel_downloads>3</nb_parallel_downloads>
      <nb_retries>3</nb_retries>
      <delay_between_downloads>0</delay_between_downloads>
      <delay_between_scans>3600</delay_between_scans>
      <max_nb_of_lines_in_auto_listing>100</max_nb_of_lines_in_auto_listing>
      <delay_before_download_start>30</delay_before_download_start>
      <check_source_availability>false</check_source_availability>
      <max_nb_of_concurrent_streams>30</max_nb_of_concurrent_streams>
      <monitoring/>
      <project/>
      <protocol_timeout/>
    </download_settings>
    <debug/>
  </download_config>



Each file starts with a < **download_config**> root markup with an id attribute defining
the download identifier. Use here the same download identifier as in the filename.



The main sections of this XML configuration file are :




+------------------------------------+---------+---------------------------------------------------------------------------------------------------------+
|             **section**            |**M / O**|                                                **Usage**                                                |
+====================================+=========+=========================================================================================================+
| <**data_source**>                  | M       | describes the source of the data files to collect :                                                     |
|                                    |         |                                                                                                         |
|                                    |         | server, pattern of the files to select, etc...                                                          |
+------------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| <**data_destination**>             | M       | describes where to put the collected files and how to organise them                                     |
+------------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| <**download_settings**>            | O       | some settings to finely tune the file polling and download mechanism                                    |
+------------------------------------+---------+---------------------------------------------------------------------------------------------------------+
| <**debug**>                        | O       | force log debug mode if flag is true                                                                    |
|                                    |         |                                                                                                         |
|                                    |         | **Default :** false                                                                                     |
+------------------------------------+---------+---------------------------------------------------------------------------------------------------------+


Data_source
~~~~~~~~~~~


This sections describes where and which data files to download. It consists of the
following XML sections:




+-------------------------------------+-----------+----------------------------------------------------------------------------------------------+
|             **section**             | **M / O** |                                                **Usage**                                     |
+=====================================+===========+==============================================================================================+
| <**location**>                      | M         | The source to poll and download the data from.                                               |
|                                     |           |                                                                                              |
|                                     |           | See location chapter below.                                                                  |
+-------------------------------------+-----------+----------------------------------------------------------------------------------------------+
| <**selection**>                     | O         | This filters to finely select the relevant files to (re)download among                       |
|                                     |           |                                                                                              |
|                                     |           | all the files  stored at the source                                                          |
|                                     |           |                                                                                              |
|                                     |           | See selection chapter below.                                                                 |
|                                     |           |                                                                                              |
|                                     |           | **default :** any modified files wrt a previous scan are downloaded                          |
+-------------------------------------+-----------+----------------------------------------------------------------------------------------------+
| <**date_extraction**>               | M         | How to get the sensing time from the files at the source. It is required to                  |
|                                     |           |                                                                                              |
|                                     |           | apply selection filters related to the actual sensing time of the EO data.                   |
|                                     |           |                                                                                              |
|                                     |           |                                                                                              |
|                                     |           |                                                                                              |
|                                     |           | See date_extraction chapter below.                                                           |
+-------------------------------------+-----------+----------------------------------------------------------------------------------------------+



Location
++++++++




The <**location**> section defines the physical location of the data to download (for
instance a remote FTP server).



The following elements are available :



+--------------------+--------------+-----------------------------------------------------------------------------------------------------------------------+
|      **section**   |  **M / O**   |                                                                    **Usage**                                          |
+====================+==============+=======================================================================================================================+
| <**protocol**>     | M            | The type or protocol of access for this location must be one of the                                                   |
|                    |              |                                                                                                                       |
|                    |              | following protocol identifiers (implemented in the eo-dataflow-manager                                                |
|                    |              |                                                                                                                       |
|                    |              | through plugins).                                                                                                     |
|                    |              |                                                                                                                       |
|                    |              |                                                                                                                       |
|                    |              |                                                                                                                       |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | **Protocol Id**      | **Usage**                                                                                  | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Ftp*                | download files from a remote FTP server                                                    | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Ftps*               | download files from a remote FTPS server                                                   | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Sftp*               | download files from a remote SFTP server                                                   | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Http*               | download files from HTTP                                                                   | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | (using the hyperlinks of the returned HTTP page)                                           | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Https*              | download files from HTTPS                                                                  | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | (using the hyperlinks of the returned HTTP page)                                           | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Webdav*             | download files from WebDAV server                                                          | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Localpath*          | copy files from a local file system                                                        | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Localpointer*       | similar to *LocalCopy* except the files are not                                            | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | physically copied, symbolic links arecreated instead                                       | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Localmove*          | similar to *LocalCopy* except the files are                                                | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | physically moved                                                                           | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Onlynotify*         | Only notify when a file is new or updated                                                  | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | on the local file system                                                                   | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Localnotification*  | similar to *LocalCopy* except the files are not                                            | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | physically copied, notifications with the                                                  | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | file path are created instead                                                              | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Opensearch*         | download files from the URLs returned by an                                                | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | Opensearch service (such as DataHub used by                                                | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | ESA or Eumetsat for Sentinel data)                                                         | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Https_xx_daac*      | download files stored in the new “earthdata” cloud                                         | |
|                    |              | |                      |                                                                                            | | 
|                    |              | |                      | infrastructure using their specific API                                                    | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Https_eop_os*       | download files stored in the new “eumetsat”                                                | |
|                    |              | |                      |                                                                                            | | 
|                    |              | |                      | datastore infrastructure using  specific API                                               | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *https_listing_file* | List of files to be downloaded is provided by a                                            | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | specific file. To download any file the protocol uses                                      | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | information of the file to download it from specific                                       | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | link                                                                                       | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *https_s3_cmems*     | download files stored in the new “Copernicus Marine Data Store”                            | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | Download files from s3 serveur                                                             | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
|                    |              | | *Lslr_file*          | Data is provided by a local file containing a list of                                      | |
|                    |              | |                      |                                                                                            | |
|                    |              | |                      | file informations.                                                                         | |
|                    |              | +----------------------+--------------------------------------------------------------------------------------------+ |
+--------------------+--------------+-----------------------------------------------------------------------------------------------------------------------+



**Ftp**
```````


For a FTP server (< **protocol**>FTP</ **protocol**>), the following markups can also be
used:




+-----------------------------------+-----------+---------------------------------------------------------------------------------------------------+
|            **section**            | **M / O** |                                             **Usage**                                             |
+===================================+===========+===================================================================================================+
| <**server**>                      | M         | the remote FTP server URL                                                                         |
+-----------------------------------+-----------+---------------------------------------------------------------------------------------------------+
| <**login**>                       | O         | the login of the ftp account ("anonymous" if none is required)                                    |
|                                   |           |                                                                                                   |
|                                   |           | **default :** anonymous                                                                           |
+-----------------------------------+-----------+---------------------------------------------------------------------------------------------------+
| <**password**>                    | O         | the password of the ftp account                                                                   |
|                                   |           |                                                                                                   |
|                                   |           | **default :** anonymous                                                                           |
+-----------------------------------+-----------+---------------------------------------------------------------------------------------------------+
| <**rootpath**>                    | O         | the path to the root directory where the data are stored                                          |
|                                   |           |                                                                                                   |
|                                   |           | **default :** /                                                                                   |
+-----------------------------------+-----------+---------------------------------------------------------------------------------------------------+
| <**port**>                        | O         | **default :** 21                                                                                  |
+-----------------------------------+-----------+---------------------------------------------------------------------------------------------------+
| <**protocol_option**>             | O         | allows modification of protocol behavior                                                          |
|                                   |           |                                                                                                   |
|                                   |           |                                                                                                   |
|                                   |           | +--------------------------+-----------------------------------------+                            |
|                                   |           | | **Option**               | **Values**                              |                            |
|                                   |           | +--------------------------+-----------------------------------------+                            |
|                                   |           | | "FollowLinks"            | "true" / "false", **default:** "false"  |                            |
|                                   |           | +--------------------------+-----------------------------------------+                            |
|                                   |           | | "SkipPermissions"        | "true" / "false", **default:** "false"  |                            |
|                                   |           | +--------------------------+-----------------------------------------+                            |
|                                   |           |                                                                                                   |
|                                   |           | **not used by default**                                                                           |
+-----------------------------------+-----------+---------------------------------------------------------------------------------------------------+





*Example*:

Here is the configuration used to download the IMERG NRT data from NASA
(`https://pmm.nasa.gov/data-access/downloads/gpm`__).



.. code-block:: html

  <location>
    <protocol>Ftp</protocol>
    <protocol_option>FollowLinks": "true"</protocol_option> 
    <server>ftp.nodc.noaa.gov</server>
    <login>anonymous</login>
    <password>fpaf@ifremer.fr</password>
    <rootpath>/pub/data.nodc/ndbc/cmanwx</rootpath>
  </location>


__ https://pmm.nasa.gov/data-access/downloads/gpm


**Https**
`````````


For HTTPS servers, specific html parsers are often useful to analyze the html code
returned. The "protocol_option" tag can be added to indicate the parser to use:

<**protocol option**>'FileExtractor': 'Specific_html_parser'</**protocol_option**>

allow values :
 - **Nomads**
 - **RdaUcar_Thredds**
 - **UniBremen**
 - **UniHamburg**

+----------------------+-------------------------------------------------------------------------------------------------------------+
|  **FileExtractor**   |                                                **Source site example**                                      |
+======================+=============================================================================================================+
| **Nomads**           | Server : https://daacdata.apps.nsidc.org/                                                                   |
|                      |                                                                                                             |
|                      | .. image:: _static/nomads.png                                                                               |
|                      |   :width: 350                                                                                               |
+----------------------+-------------------------------------------------------------------------------------------------------------+
| **RdaUcar_Thredds**  | Server : https://rda.ucar.edu/                                                                              |
|                      |                                                                                                             |
|                      | .. image:: _static/ucar_thredds.png                                                                         |
|                      |   :width: 500                                                                                               |
+----------------------+-------------------------------------------------------------------------------------------------------------+
| **UniBremen**        | Server : https://seaice.uni-bremen.de/                                                                      |
|                      |                                                                                                             |
|                      | .. image:: _static/uni-bremen.png                                                                           |
|                      |   :width: 350                                                                                               |
+----------------------+-------------------------------------------------------------------------------------------------------------+
| **UniHamburg**       | Server : https://icdc.cen.uni-hamburg.de/                                                                   |
|                      |                                                                                                             |
|                      | .. image:: _static/uni-hamburg.png                                                                          |
|                      |   :width: 600                                                                                               |
+----------------------+-------------------------------------------------------------------------------------------------------------+

In addition, if authentication is performed on a specific server, it is necessary to
reference this server:

<**protocol option**>'Redirector': 'authentification_server'</**protocol_option**>

or / and

<**protocol option**>'UrlLogin': 'authentification_url'</**protocol_option**>

The different inputs of "protocol_options" must be separated by commas.

*Example*:

Here is the configuration used to download the GES DISC data from NASA
(`https://disc2.gesdisc.eosdis.nasa.gov/data/TRMM_L3/TRMM_3B42/`__)


.. code-block:: html

  <location>
    <protocol>Https</protocol>
    <protocol_option>{'FileExtractor': 'Nomads',
                      'Redirector':'urs.earthdata.nasa.gov'
                     }</protocol_option>
    <server>disc2.gesdisc.eosdis.nasa.gov</server>
    <login>user</login>
    <password>passwd</password>
    <rootpath>/data/TRMM_L3/TRMM_3B42</rootpath>
  </location>


__ `https://disc2.gesdisc.eosdis.nasa.gov/data/TRMM_L3/TRMM_3B42/`


**Https_xx_daac**
`````````````````

For Distributed Active Archive Center, a specific protocol, *https_xx_daac*, must be used to search file and then downloading it via https requests, the following markups can also be
used:



+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
|              **section**               | **M / O**  |                                                   **Usage**                                                    |
+========================================+============+================================================================================================================+
| <**server**>                           | M          | Earthdata HTTPS server URL                                                                                     |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**login**>                            | O          | the login of the Earthdata account                                                                             |
|                                        |            | **default :** anonymous                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**password**>                         | O          | the password of the Earthdata account                                                                          |
|                                        |            | **default :** anonymous                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**rootpath**>                         | M          | Chosen search API’s path                                                                                       |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**protocol_option**>                  | M          | allows modification of protocol behavior                                                                       |
|                                        |            |                                                                                                                |
|                                        |            |                                                                                                                |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | **Option**                              | **Values**                                                       | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | "Provider"                              | Mandatory. Example:                                              | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | 'POCLOUD', 'OB_DAAC', 'PO_DAAC'                                  | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | "ShortName"                             | Mandatory. Example:                                              | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | 'MODIS_A-JPL-L2P-v2019.0'                                        | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | "Redirector"                            | Authentication server. Example:                                  | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | 'urs.earthdata.nasa.gov'                                         | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | PageSize                                | API result page size                                             | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | **default:**'2000'                                               | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | SortKey                                 | Sort key of the files list. Example:                             | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | 'StartDate'                                                      | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | DateFormat                              | Final format of used dates in query                              | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | **default:**'yyyy-MM-ddTHH:mm:ssZ'                               | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | StartDate                               | Search files with dates after StartDate                          | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | Possible input formats:                                          | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | => 'now - 2d',                                                   | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | => '2022-009-30T00:00:00Z'                                       | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | => '1 month ago'.                                                | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | EndDate                                 | Search files with dates before EndDate                           | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | (see input formats of the StartDate option)                      | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | UpdatedSince                            | Search files updated since a specified date                      | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | (see input formats of the StartDate option)                      | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | BoundingBox                             | Bounding box for file search                                     | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | (W/S corner + E/N corner). Example:                              | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | '30.0,-20.5,61,14.05'                                            | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | Polygon                                 | Polygon for file search                                          | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | Points (lon/lat) describing the polygon.                         | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | The last point must be equal to the first point .                | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | Example:                                                         | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | '11,11,31,11,31,21,11,21,11,11'                                  | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | Appkey                                  | Credential key for OB.DAAC access (mandatory)                    | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            |                                                                                                                |
|                                        |            | **not used by default**                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+



*Example*:

.. code-block:: html

  <location> 
    <protoco>https_xx_daac</protocol>
    <protocol_option>{'Provider' : 'POCLOUD',
                      'ShortName' : 'MODISA_L3m_CHL_NRT',
                      'Redirector': 'urs.earthdata.nasa.gov',
                      'PageSize': 2000,
                      'SortKey': 'start_date',
                      'DateFormat': 'yyyy-MM-ddTHH:mm:ssZ',
                      'StartDate': 'Now - 4d',
                      'EndDate': '' ,
                      'UpdatedSince': '1 day ago'
                     }</protocol_option>
    <server>ftp.nodc.noaa.gov</server>
    <login>anonymous</login>
    <password>anonymous</password>
    <rootpath>/pub/data.nodc/ndbc/cmanwx</rootpath>
  </location>


**Https_eop_os**
`````````````````

For Distributed Active Archive Center, a specific protocol, *https_eop_os*, must be used to search file and then downloading it via https requests, the following markups can also be
used:



+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
|              **section**               | **M / O**  |                                                   **Usage**                                                    |
+========================================+============+================================================================================================================+
| <**server**>                           | M          | Earthdata HTTPS server URL                                                                                     |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**login**>                            | O          | the login of the Earthdata account                                                                             |
|                                        |            | **default :** anonymous                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**password**>                         | O          | the password of the Earthdata account                                                                          |
|                                        |            | **default :** anonymous                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**rootpath**>                         | M          | Chosen search API’s path                                                                                       |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**protocol_option**>                  | M          | allows modification of protocol behavior                                                                       |
|                                        |            |                                                                                                                |
|                                        |            |                                                                                                                |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | **Option**                              | **Values**                                                       | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | ParentIdentifier                        | Collection Identifier. Mandatory. Example:                       | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | 'EO:EUM:DAT:0412'                                                | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | PlatformShortName                       | Mission / Satellite. Example:                                    | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | 'Sentinel-3A'                                                    | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | type                                    | Product Type. Example:                                           | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | 'SL_2_WST___'                                                    | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | PageSize                                | API result page size                                             | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | **default:**'100',   **max**:1000                                | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | StartDate                               | Search files with dates after StartDate                          | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | Possible input formats:                                          | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | => 'now - 2d',                                                   | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | => '2022-009-30T00:00:00Z'                                       | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | => '1 month ago'.                                                | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | EndDate                                 | Search files with dates before EndDate                           | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | (see input formats of the StartDate option))                     | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | UpdatedSince                            | Search files updated since a specified date                      | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | (see input formats of the StartDate option))                     | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | BoundingBox                             | Bounding box for file search                                     | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | (W/S corner + E/N corner). Example:                              | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | '30.0,-20.5,61,14.05'                                            | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | Geometry                                | WKT geometry                                                     | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | WARNING: replace space characters                                | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         |  with the sequence '%20'                                         | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | Example:                                                         | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | 'POLYGON((10%2056,11%2056,11%2056.2,10%2056.2,10%2056))'         | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | SortKey                                 | Sort key of the files list                                       | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | DateFormat                              | Final format of used dates in query                              | |
|                                        |            | |                                         |                                                                  | |
|                                        |            | |                                         | **default:**'yyyy-MM-ddTHH:mm:ssZ'                               | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            | | PageSize                                | API result page size                                             | |
|                                        |            | +-----------------------------------------+------------------------------------------------------------------+ |
|                                        |            |                                                                                                                |
|                                        |            | **not used by default**                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+


*Example*:

.. code-block:: html

  <location>
    <protoco>https_xx_daac</protocol>
    <protocol_option>{'ParentIdentifier': 'EO:EUM:DAT:0412',
                      'PlatformShortName': 'Sentinel-3B',
                      'PageSize': 1000,
                      'DateFormat': 'yyyy-MM-ddTHH:mm:ssZ',
                      'StartDate': '2022-01-01T01:02:03',
                      'UpdatedSince': 'now - 6h',
                      'Geometry': POLYGON ((10.09 56.09, 10.34 56.09, 10.34 56.19, 10.09 56.19, 10.09 56.09))'
                     }</protocol_option>
    <server>api.eumetsat.int</server>
    <login>xxxxxxxxxxxxxxxxxxxxxxxx</login>
    <password>zzzzzzzzzzzzzzzzzzz</password>
    <rootpath>data/search-products/os</rootpath>
  </location>


***Https_listing_file**
````````````````````````


For the U.S.NAVAL RESEARCH LABORATORY data, a specific protocol, *https_listing_file*, must be used to get file list from a
*.list* file and then downloading it via https requests, the following markups can also be
used:



+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
|              **section**               | **M / O**  |                                                   **Usage**                                                    |
+========================================+============+================================================================================================================+
| <**server**>                           | M          | HTTPS server URL                                                                                               |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**login**>                            | O          | the login if exists                                                                                            |
|                                        |            | **default :** anonymous                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**password**>                         | O          | the password of the account                                                                                    |
|                                        |            | **default :** anonymous                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**rootpath**>                         | M          | Direction to the . *list* file                                                                                 |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**protocol_option**>                  | O          | allows modification of protocol behavior                                                                       |
|                                        |            |                                                                                                                |
|                                        |            |                                                                                                                |
|                                        |            | +-------------------------------------+----------------------------------------------------------------------+ |
|                                        |            | | **Option**                          | **Values**                                                           | |
|                                        |            | +-------------------------------------+----------------------------------------------------------------------+ |
|                                        |            | | "DataServerAddress"                 | First part of url for download. Example:                             | |
|                                        |            | |                                     |                                                                      | |
|                                        |            | |                                     | "https://www.nrlmry.navy.mil/atcf_web/docs/tracks/1997"              | |
|                                        |            | +-------------------------------------+----------------------------------------------------------------------+ |
|                                        |            | | "CheckCertificate"                  | "true" / "false"                                                     | |
|                                        |            | |                                     |                                                                      | |
|                                        |            | |                                     | **default:** "true"                                                  | |
|                                        |            | +-------------------------------------+----------------------------------------------------------------------+ |
|                                        |            |                                                                                                                |
|                                        |            | **not used by default**                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+




*Example*:

.. code-block:: html

  <location> 
    <protocol>https_listing_file</protocol> 
    <protocol_option>{'DataServerAddress': 'https://www.nrlmry.navy.mil/atcf_web/docs/tracks/1997', 'CheckCertificate' : false} 
  </protocol_option> 
    <server>www.nrlmry.navy.mil</server>
    <login>anonymous</login>
    <password>anonymous</password>
    <rootpath>/atcf_web/docs/tracks/tracks_2022.list</rootpat>
  </location>


***Https_s3_cmems**
````````````````````````


For the Copernicus Marine Data Store, a specific protocol, *https_s3_cmems*, must be used to get file list from s3 buckets and then downloading it via s3 requests, the following markups can also be
used:



+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
|              **section**               | **M / O**  |                                                   **Usage**                                                    |
+========================================+============+================================================================================================================+
| <**server**>                           | M          | HTTPS server URL                                                                                               |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**login**>                            | O          | the login if exists                                                                                            |
|                                        |            | **default :** anonymous                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**password**>                         | O          | the password of the account                                                                                    |
|                                        |            | **default :** anonymous                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**rootpath**>                         | M          | bucket and path of the dataset                                                                                 |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+
| <**protocol_option**>                  | O          | **not used by default**                                                                                        |
+----------------------------------------+------------+----------------------------------------------------------------------------------------------------------------+




*Example*:

.. code-block:: html

  <location>
    <protocol>https_s3_cmems</protocol>
    <server>s3.waw3-1.cloudferro.com</server>
    <login>anonymous</login>
    <password>anonymous</password>
    <rootpath>mdl-native-06/native/SST_GLO_SST_L4_NRT_OBSERVATIONS_010_001/METOFFICE-GLO-SST-L4-NRT-OBS-SST-V2</rootpat>
  </location>


Selection
+++++++++


The <**selection**> section defines which files must be downloaded on the source location. By
default all files will be downloaded if this section is not present in the configuration
file.



The selection can be performed through various filters and criteria that add up to each
other (AND relation). They provide much more advanced settings to refine the exact
conditions on which data files should be downloaded (or redownloaded), to minimize useless
download and avoid possible bandwidth or local storage bottlenecks.








+------------------------------------------+-----------+----------------------------------------------------------------------------------+
|                    **section**           | **M / O** |                                                          **Usage**               |
+==========================================+===========+==================================================================================+
| <**update**>                             | O         | Defines which files must be selected wrt to previous scan of the source.         |
|                                          |           |                                                                                  |
|                                          |           | This takes one of the following values:                                          |
|                                          |           |                                                                                  |
|                                          |           | +-------------------+----------------------------------------------------------+ |
|                                          |           | | new               | Any file that was not present on the source at the       | |
|                                          |           | |                   |                                                          | |
|                                          |           | |                   | previous scan (new files)                                | |
|                                          |           | +-------------------+----------------------------------------------------------+ |
|                                          |           | | modified          | Any file that was not previous or which                  | |
|                                          |           | |                   |                                                          | |
|                                          |           | |                   | modification time was different at the previous          | |
|                                          |           | |                   |                                                          | |
|                                          |           | |                   | scan (new and modified files).                           | |
|                                          |           | +-------------------+----------------------------------------------------------+ |
|                                          |           |                                                                                  |
|                                          |           | **default:** modified                                                            |
+------------------------------------------+-----------+----------------------------------------------------------------------------------+
| <**files**>                              | O         | See files chapter below.                                                         |
|                                          |           |                                                                                  |
|                                          |           | **default :** all files                                                          |
+------------------------------------------+-----------+----------------------------------------------------------------------------------+
| <**directories**>                        | O         | See directories chapter below.                                                   |
|                                          |           |                                                                                  |
|                                          |           | **default:** all directories                                                     |
+------------------------------------------+-----------+----------------------------------------------------------------------------------+
| <**date_folders**>                       | O         | Defines which sensing time related folders will be scanned on the                |
|                                          |           |                                                                                  |
|                                          |           | source (when data are organized in year, month, etc subfolders).                 |
|                                          |           |                                                                                  |
|                                          |           | This allows to limit the scanning time of theremote source,                      |
|                                          |           |                                                                                  |
|                                          |           | which can be a very expensive operation, by only polling a limited               |
|                                          |           |                                                                                  |
|                                          |           | number of folders instead of thewhole directory structure.                       |
|                                          |           |                                                                                  |
|                                          |           |                                                                                  |
|                                          |           | See date_folders chapter below.                                                  |
|                                          |           |                                                                                  |
|                                          |           | **default:** the whole remote directory structure is scanned                     |
+------------------------------------------+-----------+----------------------------------------------------------------------------------+
| <**opensearch**>                         | M / O     | Mandatory when Opensearch is set as source protocol, not use                     |
|                                          |           |                                                                                  |
|                                          |           | otherwise.                                                                       |
|                                          |           |                                                                                  |
|                                          |           | See opensearch chapter below.                                                    |
+------------------------------------------+-----------+----------------------------------------------------------------------------------+
| <**geotemporal**>                        | O         | Optional and only applicable for the xx_daac and eop_os protocols,               |
|                                          |           |                                                                                  |
|                                          |           | not use otherwise.                                                               |
|                                          |           |                                                                                  |
|                                          |           | See geotemporal chapter below.                                                   |
+------------------------------------------+-----------+----------------------------------------------------------------------------------+



**date_folders**
````````````````


The folders to scan for new files on the source can be defined through the following
elements:






+-----------------------------+-----------+-----------------------------------------------------------------------------------------+
|          **section**        | **M / O** |                                        **Usage**                                        |
+=============================+===========+=========================================================================================+
| <**pattern**>               | M         | The pattern of the date subfolder names (using python datetime                          |
|                             |           |                                                                                         |
|                             |           | format). At least one of the following options must be defined                          |
|                             |           |                                                                                         |
|                             |           | to determine the scan interval.                                                         |
+-----------------------------+-----------+-----------------------------------------------------------------------------------------+
| <**backlog_in_days**>       | O         | The number of days (from current time) to scan backward.                                |
|                             |           |                                                                                         |
|                             |           | Only the date subfolders corresponding to these last N days will be                     |
|                             |           |                                                                                         |
|                             |           | scanned.                                                                                |
|                             |           |                                                                                         |
|                             |           | **not used by default**                                                                 |
+-----------------------------+-----------+-----------------------------------------------------------------------------------------+
| <**max_date**>              | O         | The date of the oldest date folder to scan. Folders corresponding                       |
|                             |           |                                                                                         |
|                             |           | to dates olders that this date will not be scanned ever.                                |
|                             |           |                                                                                         |
|                             |           | **not used by default**                                                                 |
+-----------------------------+-----------+-----------------------------------------------------------------------------------------+
| <**min_date**>              | O         | The date of the oldest date folder to scan. Folders corresponding                       |
|                             |           |                                                                                         |
|                             |           | to dates olders thatthis date will not be scanned ever.                                 |
|                             |           |                                                                                         |
|                             |           | **not used by default**                                                                 |
+-----------------------------+-----------+-----------------------------------------------------------------------------------------+





*Example*:



.. code-block:: html

  <date_folders> 
    <pattern></pattern>
    <backlog_in_days></backlog_in_days>
    <max_date></max_date>
    <min_date></min_date>
  </date_folders>


**files**
`````````


+--------------------------------------+-----------+-----------------------------------------------------------------------+
|             **section**              | **M / O** |                                               **Usage**               |
+======================================+===========+=======================================================================+
| <**regexp**>                         | O         | The pattern of the files to select, using a regular                   |
|                                      |           |                                                                       |
|                                      |           | expression                                                            |
|                                      |           |                                                                       |
|                                      |           | **default :** all files                                               |
+--------------------------------------+-----------+-----------------------------------------------------------------------+
| <**ignore_regexp**>                  | O         | The pattern of the files **not to** select, using a                   |
|                                      |           |                                                                       |
|                                      |           | regular expression                                                    |
|                                      |           |                                                                       |
|                                      |           | **not used by default**                                               |
+--------------------------------------+-----------+-----------------------------------------------------------------------+
| <**ignore_modify_time_older_than**>  | O         | Ignore the files which modification time is older than                |
|                                      |           |                                                                       |
|                                      |           | the specified limit expressed in days.                                |
|                                      |           |                                                                       |
|                                      |           | **not used by default**                                               |
+--------------------------------------+-----------+-----------------------------------------------------------------------+



*Example:*



.. code-block:: html

  <files> 
    <regexp/>
    <ignore_modify_time_older_than>4</ignore_modify_time_older_than>
    <ignore_regexp></ignore_regexp> 
  </files>





**directories**
```````````````







+--------------------------------------+-----------+-----------------------------------------------------------------------+
|             **section**              | **M / O** |                                                **Usage**              |
+======================================+===========+=======================================================================+
| <**regexp**>                         | O         | The pattern of the directories to select, using a                     |
|                                      |           |                                                                       |
|                                      |           | regular expression                                                    |
|                                      |           |                                                                       |
|                                      |           | **default:** all directories                                          |
+--------------------------------------+-----------+-----------------------------------------------------------------------+
| <**ignore_regexp**>                  | O         | The pattern of the directories **not to** select, using               |
|                                      |           |                                                                       |
|                                      |           | a regular expression                                                  |
|                                      |           |                                                                       |
|                                      |           | **not used by default**                                               |
+--------------------------------------+-----------+-----------------------------------------------------------------------+
| <**ignore_modify_time_older_than**>  | O         | Ignore the directories which modify time is older than                |
|                                      |           |                                                                       |
|                                      |           | the specified limit expressed in days.                                |
|                                      |           |                                                                       |
|                                      |           | **not used by default**                                               |
+--------------------------------------+-----------+-----------------------------------------------------------------------+
| <**ignore_newer_than>**              | O         | Ignore the directories which modification time is                     |
|                                      |           |                                                                       |
|                                      |           | more recent than this value, expressed in minutes.                    |
|                                      |           |                                                                       |
|                                      |           | iThis allows to avoid downloading folders that may                    |
|                                      |           |                                                                       |
|                                      |           | still be being filled in bythe source provider.                       |
|                                      |           |                                                                       |
|                                      |           | **not used by default**                                               |
+--------------------------------------+-----------+-----------------------------------------------------------------------+



*Example:*


.. code-block:: html

  <directories>
    <ignore_newer_than>60</ignore_newer_than> 
    <ignore_modify_time_older_than>4</ignore_modify_time_older_than>
    <ignore_regexp></ignore_regexp>
    <regexp></regexp>
  </directories> 


**opensearch**
``````````````


Only applicable when Opensearch is set as source protocol.






+---------------------------+-----------+-----------------------------------------------------------------------------+
|        **section**        | **M / O** |                                  **Usage**                                  |
+===========================+===========+=============================================================================+
| <**area**>                | O         | The area over which to query the available products.                        |
|                           |           |                                                                             |
|                           |           | **not used by default**                                                     |
+---------------------------+-----------+-----------------------------------------------------------------------------+
| <**dataset**>             | M         | The platform, the instrument, the date range, the data type... to select.   |
+---------------------------+-----------+-----------------------------------------------------------------------------+
| <**request_format**>      | O         | The presentation options.                                                   |
|                           |           |                                                                             |
|                           |           | **default:** &orderby=beginposition%20desc                                  |
|                           |           |                                                                             |
|                           |           | **(must not be filled)**                                                    |
+---------------------------+-----------+-----------------------------------------------------------------------------+



*Example:*



.. code-block:: html

  <opensearch> 
    <area></area>
    <dataset>platformname:Sentinel-3 AND producttype:SR_2_WAT___ AND timeliness:"Near Real Time"</dataset>
    <request_format></request_format>
  </opensearch>                                                                     |


**geotemporal**
```````````````


Only applicable when xx_daac or eop_os is set as source protocol.




+---------------------------+-----------+-----------------------------------------------------------------------------+
|        **section**        | **M / O** |                                  **Usage**                                  |
+===========================+===========+=============================================================================+
| <**geotemporal_list**>    | O         | The path of the file containing a list of geometry/time                     |
|                           |           |                                                                             |
|                           |           | interval pairs in json format                                               |
|                           |           |                                                                             |
|                           |           | **not used by default**                                                     |
+---------------------------+-----------+-----------------------------------------------------------------------------+


If this markup is specified, the protocol_option "start_date", "end_date", "bbox" and "geometry" will be ignored.



*Example:*



.. code-block:: html

  <geotemporal>
    <geotemporal_list>/data/eo-dataflow/geotemporal_filter/list.json</geotemporal_list>
  </geotemporal>



*File content example:*


.. code-block:: json

 [
   {
     "geometry": "POLYGON((-6.5 49,-6.5 47.5,-4 47.5,-4 49,-6.5 49))",
     "start_date": "2023-12-10 00:00",
     "end_date": "2023-12-11 00:00"
   },
   {
     "start_date": "2023-12-10 12:00",
     "end_date": "2023-12-11 12:00",
     "bounding_box": "-8,47,-5,48.5"
   },
   {
     "geometry": "",
     "start_date": "2023-12-11 00:00",
     "end_date": "2023-12-12 00:00",
     "bounding_box": "-9.5,46,-6,48"
   },
   {
     "start_date": "2023-12-11 12:00",
     "end_date": "2023-12-12 12:00",
     "bounding_box": "-11,45,-7,47"
   }
 ]


Date_extraction
+++++++++++++++


The <**date_extraction**> section defines how to extract the sensing date of a EO data
file. This can be performed through various methods, available through plugins.




+-------------------------------+-----------+-------------------------------------------------------------------------+
|          **section**          | **M / O** |                                       **Usage**                         |
+===============================+===========+=========================================================================+
| <**regexp**>                  | M         | defines how to extract the acquisition date string from the filename.   |
+-------------------------------+-----------+-------------------------------------------------------------------------+
| <**format**>                  | M         | the time format (using python datetime format string convention)        |
|                               |           |                                                                         |
|                               |           | corresponding to the extracted date string.                             |
+-------------------------------+-----------+-------------------------------------------------------------------------+



**RegexpDataReader**
````````````````````


In most cases, when the data filenames contain their acquisition time, the files can be
selected with a simple selection pattern (regular expression) built from the selection
date, using the RegexpDataReader plugin. This selection pattern is defined with the
following XML elements:



*Example*:

In our previous example, for IMER data, the files to download are named as follow:

3B-HHR-L.MS.MRG.3IMERG.20180708-S173000-E175959.1050.V05B.RT-H5

The file selection by date can therefore be configured through a regular expression
pattern, as follow:



.. code-block:: html

  <date_extraction plugin_name="RegexpDataReader"> 
    <regexp>3B-HHR-L\.MS\.MRG\.3IMERG\.([0-9]{8}-S[0-9]{6}).*</ regexp> 
    <format>%Y%m%d-S%H%M%S</format>
  </date_extraction>                                                       |



**DirRegexpDataReader**
```````````````````````


*Example*:

For Sentinel 3 WCT data files, the name of the file directory contains the detection date,
the files to download are named as follows:

S3A_SL_2_WCT____20181204053103_20181204T053403_20181204T071835_0179_038_347_4320_MAR_O_NR_003.SEN3/met_tx.nc

Directory selection by date can therefore be configured using a regular expression
pattern, as follows:





.. code-block:: html

  <date_extraction plugin="DirRegexpDataReader"> 
    <regexp>S3A_SL_2_WCT____([0-9]{8}).*</regexp>
    <format>%Y%m%dT%H%M%S</format>
  </date_extraction>


**NetCDFDataReader**
````````````````````


The NetCDFDatareader plug-in allows you to retrieve the detection date from the header
attributes of a NetCDF file. In this case, the name of the attribute is placed in the
regexp element, as follows:




.. code-block:: html

  <date_extraction plugin="NetCDFDataReader">
    <regexp>first_meas_time</regexp> 
    <format>%Y-%m-%d %H:%M:%S.%f</format>
  </date_extraction>



**GribDataReader**
``````````````````


The GribDatareader plug-in allows you to retrieve the detection date from the forecast
date of a GRIB file. In this case, the regexp and format elements are not used but must be
present and can remain empty, as follows:



.. code-block:: html

  <date_extraction plugin="GribDataReader">
    <regexp></regexp>
    <format></format>
  </date_extraction>




**TreeDataReader**
``````````````````


The TreeDataReader plugin does not extract dates but allows the tree structure of source files
to be preserved when they are made available to the destination.
In this case, the regexp and format elements are not used but must be
present and can remain empty, as follows:



.. code-block:: html

  <date_extraction plugin="TreeDataReader">
    <regexp></regexp>
    <format></format>
  </date_extraction>




Data destination
~~~~~~~~~~~~~~~~


This sections describes where and how to store the downloaded data. It consists of the
following XML sections:




+--------------------------------+-----------+------------------------------------------------------------------------+
|          **section**           | **M / O** |                                         **Usage**                      |
+================================+===========+========================================================================+
| <**location**>                 | M         | Defines the main destination of the downloaded data files              |
|                                |           |                                                                        |
|                                |           | as well as others (secondary) destination                              |
+--------------------------------+-----------+------------------------------------------------------------------------+
| <**optional_spool_location**>  | O         | Defines optional locations where to store the downloaded               |
|                                |           |                                                                        |
|                                |           | data files                                                             |
+--------------------------------+-----------+------------------------------------------------------------------------+
| <**organization**>             | O         | Defines how to store the data (folder structure).                      |
|                                |           |                                                                        |
|                                |           | See organization chapter below.                                        |
|                                |           |                                                                        |
|                                |           | **default :** no subfolders are created                                |
+--------------------------------+-----------+------------------------------------------------------------------------+
| <**post-processing**>          | O         | Specifies some operations (integrity check, uncompression,             |
|                                |           |                                                                        |
|                                |           | untar,...) to be applied before storing the data in their              |
|                                |           |                                                                        |
|                                |           | final folder.                                                          |
|                                |           |                                                                        |
|                                |           | See post-processing chapter below.                                     |
|                                |           |                                                                        |
|                                |           | **default :** no operation applied to the collected files              |
+--------------------------------+-----------+------------------------------------------------------------------------+



*Example:*



.. code-block:: html

  <location>/path/to/final/destination/d1</location> 
  <location>/path/to/final/destination/d2</location>  

  <optional_spool_location>/path/to/optional_spool/s1</optional_spool_location> 
  <optional_spool_location>/path/to/optional_spool/s2</optional_spool_location>


Organization
++++++++++++


The <**organization**> section defines the folder structure in which to store the data files. The following elements can
be used:




+-------------------------------+-----------+--------------------------------------------------------------------------------------------------------+
|                 **section**   | **M / O** |                                                             **Usage**                                  |
+===============================+===========+========================================================================================================+
| <**type**>                    | O         | The type of subfolder structure (if any), among the following                                          |
|                               |           |                                                                                                        |
|                               |           | choices                                                                                                |
|                               |           |                                                                                                        |
|                               |           | +-------------+--------------------------------------------------------------------------------------+ |
|                               |           | | spool       | All files are put in the folder indicated by                                         | |
|                               |           | |             |                                                                                      | |
|                               |           | |             | location without any subfolder                                                       | |
|                               |           | +-------------+--------------------------------------------------------------------------------------+ |
|                               |           | | datetree    | Files are organized following date subfolders                                        | |
|                               |           | |             |                                                                                      | |
|                               |           | |             | (the daterefering to the sensing time                                                | |
|                               |           | |             |                                                                                      | |
|                               |           | |             | associated with the EO data files).                                                  | |
|                               |           | |             |                                                                                      | |
|                               |           | |             | The pattern is defined in subpath element.                                           | |
|                               |           | +-------------+--------------------------------------------------------------------------------------+ |
|                               |           |                                                                                                        |
|                               |           | **default :** spool                                                                                    |
+-------------------------------+-----------+--------------------------------------------------------------------------------------------------------+
| <**subpath**>                 | O         | The pattern of the date subfolders used to organize the data files,                                    |
|                               |           |                                                                                                        |
|                               |           | as a python datetime format string, when **type** is *datatree*                                        |
|                               |           |                                                                                                        |
|                               |           | (unused otherwise)                                                                                     |
|                               |           |                                                                                                        |
|                               |           | **default :** %Y/%j                                                                                    |
+-------------------------------+-----------+--------------------------------------------------------------------------------------------------------+
| <**keep_parent_folder**>      | O         | Will copy the file with its parent folder within the folder structure.                                 |
|                               |           |                                                                                                        |
|                               |           | This allows to keep the folder based products (such as Sentinel                                        |
|                               |           |                                                                                                        |
|                               |           | SAFE products) that are broken down in multiple files contained                                        |
|                               |           |                                                                                                        |
|                               |           | within a product container folder (the SAFE folder).                                                   |
|                               |           |                                                                                                        |
|                               |           | When SAFE products are compressed and decompression in                                                 |
|                               |           |                                                                                                        |
|                               |           | post-processing is wanted, the keep_parent_folder parameter                                            |
|                               |           |                                                                                                        |
|                               |           | must be false.                                                                                         |
|                               |           |                                                                                                        |
|                               |           | **default** : false                                                                                    |
+-------------------------------+-----------+--------------------------------------------------------------------------------------------------------+



*Example:*



.. code-block:: html

  <organization> 
    <subpath>%Y/%j</subpath>
    <type>datetree</type>
  </organization>


Post-processing
+++++++++++++++


The <**post-processing**> section defines specific operations to be applied to the files before copying them to their final location.

The following elements can be used:






+---------------------------+-----------+-------------------------------------------------------------------------+
|         **section**       | **M / O** |                                 **Usage**                               |
+===========================+===========+=========================================================================+
| <**checksum**>            | O         | Control the file integrity if a checksum is provided with the           |
|                           |           |                                                                         |
|                           |           | data files.                                                             |
|                           |           |                                                                         |
|                           |           | Use one of the following values:                                        |
|                           |           |                                                                         |
|                           |           | +---------+-----------------------------------------------------------+ |
|                           |           | | none    | No integrity check applied                                | |
|                           |           | +---------+-----------------------------------------------------------+ |
|                           |           | | md5     | Integrity check with md5 checksum (must be                | |
|                           |           | |         |                                                           | |
|                           |           | |         | provided within a file with the same name as              | |
|                           |           | |         |                                                           | |
|                           |           | |         | the data file plus a .md5 extension)                      | |
|                           |           | +---------+-----------------------------------------------------------+ |
|                           |           | | safe    | For SAFE products only. Use the checksum in               | |
|                           |           | |         |                                                           | |
|                           |           | |         | the manifest file to check the data file integrity.       | |
|                           |           | +---------+-----------------------------------------------------------+ |
|                           |           | | sha256  | Integrity check with SHA256 checksum (must                | |
|                           |           | |         |                                                           | |
|                           |           | |         | be provided within a file with the same name              | |
|                           |           | |         |                                                           | |
|                           |           | |         | as the data file plus a .sha256 extension)                | |
|                           |           | +---------+-----------------------------------------------------------+ |
|                           |           |                                                                         |
|                           |           | **default :** none                                                      |
+---------------------------+-----------+-------------------------------------------------------------------------+
| <**compression**          | O         | Handle the uncompression of the data files. Use one of                  |
|                           |           |                                                                         |
|  type="" subdir=false>    |           | Use one of the following values:                                        |
|                           |           |                                                                         |
|                           |           |                                                                         |
|                           |           | +------------+-----------------------------------------+                |
|                           |           | | none       | No compression or uncompression applied |                |
|                           |           | +------------+-----------------------------------------+                |
|                           |           | | uncompress | Uncompress files                        |                |
|                           |           | +------------+-----------------------------------------+                |
|                           |           | | compress   | Compress files                          |                |
|                           |           | +------------+-----------------------------------------+                |
|                           |           |                                                                         |
|                           |           | Use the compression **type** in the type attribute, among               |
|                           |           |                                                                         |
|                           |           | +-----+-------+                                                         |
|                           |           | | gz  | gzip  |                                                         |
|                           |           | +-----+-------+                                                         |
|                           |           | | zip | zip   |                                                         |
|                           |           | +-----+-------+                                                         |
|                           |           | | Z   | Z     |                                                         |
|                           |           | +-----+-------+                                                         |
|                           |           | | bz2 | bzip2 |                                                         |
|                           |           | +-----+-------+                                                         |
|                           |           |                                                                         |
|                           |           | Specify the addition of a container directory, in the **subdir**        |
|                           |           |                                                                         |
|                           |           |  attribute, by setting the flag to true (false by defaul)               |
|                           |           |                                                                         |
|                           |           |                                                                         |
|                           |           | **default :** none                                                      |
+---------------------------+-----------+-------------------------------------------------------------------------+
| <**archive**              | O         | Handle the extraction of "TAR" archives from data files.                |
|                           |           |                                                                         |
|  subdir=false>            |           | Use one of the following values:                                        |
|                           |           |                                                                         |
|                           |           | +---------+-------------------------+                                   |
|                           |           | | none    | No treatment is applied |                                   |
|                           |           | +---------+-------------------------+                                   |
|                           |           | | extract | Extract archive         |                                   |
|                           |           | +---------+-------------------------+                                   |
|                           |           |                                                                         |
|                           |           | Specify the addition of a container directory, in the                   |
|                           |           |                                                                         |
|                           |           | subdir attribute, by setting the flag to true                           |
|                           |           |                                                                         |
|                           |           |  (false by default)                                                     |
|                           |           |                                                                         |
|                           |           | **default :** none                                                      |
+---------------------------+-----------+-------------------------------------------------------------------------+
| <**spool_link**>          | O         | Creates a link to the downloaded file in one or more folders.           |
|                           |           |                                                                         |
|                           |           | This is intended to trigger actions on the downloaded files             |
|                           |           |                                                                         |
|                           |           |  through spools.                                                        |
|                           |           |                                                                         |
|                           |           | **default :** no link                                                   |
|                           |           |                                                                         |
|                           |           | *Example*:                                                              |
|                           |           |                                                                         |
|                           |           | <spool_link>link_path1</spool_link>                                     |
|                           |           |                                                                         |
|                           |           | <spool_link>link_path2</spool_link>                                     |
+---------------------------+-----------+-------------------------------------------------------------------------+
| <**file_group_name**>     | O         | Change the user group of a file. Indicate the name of                   |
|                           |           |                                                                         |
|                           |           |   the new user group.                                                   |
|                           |           |                                                                         |
|                           |           | **default :** no change                                                 |
+---------------------------+-----------+-------------------------------------------------------------------------+
| <**validation**>          | O         | Handle the uncompression of the data files.                             |
|                           |           |                                                                         |
|                           |           | Use one of the following values:                                        |
|                           |           |                                                                         |
|                           |           | +------------+-------------------------------------------------------+  |
|                           |           | | none       | no validation applied                                 |  |
|                           |           | +------------+-------------------------------------------------------+  |
|                           |           | | size       | checks that the size of each file is > 0              |  |
|                           |           | +------------+-------------------------------------------------------+  |
|                           |           | | netcdf     | checks size and tests opening .nc file in NetCDF      |  |
|                           |           | +------------+-------------------------------------------------------+  |
|                           |           |                                                                         |
|                           |           | **default :** no validation applied                                     |
+---------------------------+-----------+-------------------------------------------------------------------------+
| <**files_filter**>        | O         | Specifies filtering of files after extraction or decompression          |
|                           |           |                                                                         |
|                           |           | See files filter chapter below.                                         |
|                           |           |                                                                         |
|                           |           | **default :** all files are preserved                                   |
+---------------------------+-----------+-------------------------------------------------------------------------+

**files filter**
````````````````



+--------------------------------------+-----------+-----------------------------------------------------------------------+
|             **section**              | **M / O** |                                               **Usage**               |
+======================================+===========+=======================================================================+
| <**regexp**>                         | O         | The pattern of the files **to keep**, using a regular                 |
|                                      |           |                                                                       |
|                                      |           | expression                                                            |
|                                      |           |                                                                       |
|                                      |           | **default :** all files are preserved                                 |
+--------------------------------------+-----------+-----------------------------------------------------------------------+
| <**ignore_regexp**>                  | O         | The pattern of the files **to delete** , using a                      |
|                                      |           |                                                                       |
|                                      |           | regular expression                                                    |
|                                      |           |                                                                       |
|                                      |           | **not used by default**                                               |
+--------------------------------------+-----------+-----------------------------------------------------------------------+


*Example:*





.. code-block:: html

  <post-processing> 
    <checksum>md5</checksum>
    <compression type="zip" subdir=false>none</compression> 
    <archive subdir=false>none</archive>
    <spool_link>/path/to/link/spool/s1</spool_link>
    <spool_link>/path/to/link/spool/s2</spool_link>
    <files_filter>
      <regexp>.*\.nc</regexp>
    </files_filter>
  </post-processing>



Download_settings
~~~~~~~~~~~~~~~~~


The **download_settings** section allows to fine tune the way the download processing is
handled by the eo-dataflow-manager, to optimize the download performances and behaviour.



The following settings are available:






+---------------------------------------------+-----------+-------------------------------------------------------+
|                 **section**                 | **M / O** |                **Usage**                              |
+=============================================+===========+=======================================================+
| <**database**>                              | O         | The eo-dataflow-manager uses internally               |
|                                             |           |                                                       |
|                                             |           | databases to store the status of the source           |
|                                             |           |                                                       |
|                                             |           | content wrt what has been already                     |
|                                             |           |                                                       |
|                                             |           | downloaded.                                           |
|                                             |           |                                                       |
|                                             |           | These databases can grow-up quite rapidly             |
|                                             |           |                                                       |
|                                             |           | and this XML sections provides some                   |
|                                             |           |                                                       |
|                                             |           | settings to control this.                             |
|                                             |           |                                                       |
|                                             |           | See database chapter below.                           |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**check_source_availability**>             | O         | Check if the target is reachable before               |
|                                             |           |                                                       |
|                                             |           | attempting a download                                 |
|                                             |           |                                                       |
|                                             |           | **default: true**                                     |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**cycle_length**>                          | O         | Duration in seconds of the internal                   |
|                                             |           |                                                       |
|                                             |           | download cycle                                        |
|                                             |           |                                                       |
|                                             |           | **default: 60**                                       |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**nb_retries**>                            | O         | Number of times the download processing               |
|                                             |           |                                                       |
|                                             |           | will reattempt downloading a file if it fails         |
|                                             |           |                                                       |
|                                             |           | **default: 3**                                        |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**nb_parallel_downloads**>                 | O         | Number of parallel download queues for a              |
|                                             |           |                                                       |
|                                             |           | download (for instance for FTP client)                |
|                                             |           |                                                       |
|                                             |           | **default : 0**                                       |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**delay_between_downloads**>               | O         | Delay between two consecutive downloads               |
|                                             |           |                                                       |
|                                             |           | **default : 60**                                      |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**delay_before_download_start**>           | O         | Delay before actually starting a download             |
|                                             |           |                                                       |
|                                             |           | **default : 30**                                      |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**delay_between_scans**>                   | O         | Delay between to scans of the source for              |
|                                             |           |                                                       |
|                                             |           | new files to download.                                |
|                                             |           |                                                       |
|                                             |           | Use the largest possible value (depending on          |
|                                             |           |                                                       |
|                                             |           | your time constraint) to avoid too frequent           |
|                                             |           |                                                       |
|                                             |           | scans that can overload the remote server.            |
|                                             |           |                                                       |
|                                             |           |                                                       |
|                                             |           |                                                       |
|                                             |           | **default : 600**                                     |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**max_nb_of_files_downloaded_per_cycle**>  | O         | Maximum number of file downloads                      |
|                                             |           |                                                       |
|                                             |           | managed at once                                       |
|                                             |           |                                                       |
|                                             |           | **default : 24**                                      |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**max_nb_of_files_per_scan**>              | O         | Maximum number of files that can be                   |
|                                             |           |                                                       |
|                                             |           | downloaded from a new scan.                           |
|                                             |           |                                                       |
|                                             |           | This allows to prevent massive downloads              |
|                                             |           |                                                       |
|                                             |           | that may be triggered because of some                 |
|                                             |           |                                                       |
|                                             |           | change on the remote server (for instance             |
|                                             |           |                                                       |
|                                             |           | all files having their modification time              |
|                                             |           |                                                       |
|                                             |           | changed or all files being replaced by a              |
|                                             |           |                                                       |
|                                             |           | new version).                                         |
|                                             |           |                                                       |
|                                             |           | If the number of files returned by a scan             |
|                                             |           |                                                       |
|                                             |           | is greater than this limit, the files will            |
|                                             |           |                                                       |
|                                             |           | not be downloaded.                                    |
|                                             |           |                                                       |
|                                             |           | **default : 100**                                     |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**max_nb_of_concurrent_streams**>          | O         | Maximum total number of simultaneous                  |
|                                             |           |                                                       |
|                                             |           | streams,from the same source, shared with             |
|                                             |           |                                                       |
|                                             |           | all other configured "downloads".                     |
|                                             |           |                                                       |
|                                             |           | This avoids saturation of a source if several         |
|                                             |           |                                                       |
|                                             |           | "downloads" havebeen configured with this             |
|                                             |           |                                                       |
|                                             |           | source                                                |
|                                             |           |                                                       |
|                                             |           | **default : 20**                                      |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**monitoring**>                            | O         | Enable / disable sending monitoring                   |
|                                             |           |                                                       |
|                                             |           | information                                           |
|                                             |           |                                                       |
|                                             |           | **default : true**                                    |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**project**>                               | O         | Project name for the EMM reports                      |
|                                             |           |                                                       |
|                                             |           | **default : CERSAT**                                  |
+---------------------------------------------+-----------+-------------------------------------------------------+
| <**protocol_timeout**>                      | O         | timeout (in seconds) for each protocol                |
|                                             |           |                                                       |
|                                             |           | command during scanning                               |
|                                             |           |                                                       |
|                                             |           | **default : 120**                                     |
+---------------------------------------------+-----------+-------------------------------------------------------+



*Example:*



.. code-block:: html

  <download_settings> 
    <database> 
      <path></path> 
      <purge_scans_older_than></purge_scans_older_than> 
      <purge_all_scans>true|false</purge_all_scans> 
    </database>
    <check_source_availability>false</check_source_availability> 
    <delay_between_downloads>0</delay_between_downloads>  
    <nb_retries>3</nb_retries>   
    <nb_parallel_downloads>3</nb_parallel_downloads> 
    <max_nb_of_files_downloaded_per_cycle>24</max_nb_of_files_downloaded_per_cycle>
    <delay_before_download_start>30</delay_before_download_start> 
    < delay_between_scans>3600</delay_between_scans>
    <max_nb_of_activated_flows>3<max_nb_of_activated_flows>
    <monitoring>true|false</monitoring>
    <project>cersat</project>
    <protocol_timeout>300</protocol_timeout>
  </download_settings> 


Database
++++++++


To fine-tune the internal configuration of the database, use the following elements in the <**database**> section:






+-------------------------------+-----------+---------------------------------------------------------------------------------------------------+
|          **section**          | **M / O** |                                             **Usage**                                             |
+===============================+===========+===================================================================================================+
| <**path**>                    | O         | Specifies the path to the internal database file                                                  |
|                               |           |                                                                                                   |
|                               |           | **not used (default location is the internal directory of the**                                   |
|                               |           |                                                                                                   |
|                               |           | **private workspace of the download)**                                                            |
|                               |           |                                                                                                   |
+-------------------------------+-----------+---------------------------------------------------------------------------------------------------+
| <**purge_scans_older_than**>  | O         | Purge the source scans stored in the database older than                                          |
|                               |           |                                                                                                   |
|                               |           |  N days.                                                                                          |
|                               |           |                                                                                                   |
|                               |           | **not used by default**                                                                           |
+-------------------------------+-----------+---------------------------------------------------------------------------------------------------+
| <**keep_last_scan**>          | O         | Always retains the last scan performed before each new scan                                       |
|                               |           |                                                                                                   |
|                               |           | (true or false).                                                                                  |
|                               |           |                                                                                                   |
|                               |           | **true by default**                                                                               |
+-------------------------------+-----------+---------------------------------------------------------------------------------------------------+



*Example:*



.. code-block:: html

  <database>
    <path></path>
    <purge_scans_older_than>2</purge_scans_older_than>
    <keep_last_scans>true</keep_last_scan>
  </database>










