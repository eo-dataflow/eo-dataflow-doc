.. -*- coding: utf-8 -*-

Installation
============


There are two options for installing eo-dataflow-manager.

The first is an installation via conda and the second is an installation via a docker image

Installation with Conda
-----------------------

Prerequisite
~~~~~~~~~~~~


The installation procedure works with the following environment:


* python 3.8

* ubuntu 22.04 / debian 10.13

* pip 21.1.3

* miniconda 21.2.4

* git 2.25.1

* Poetry 1.3.1



Install python environment
~~~~~~~~~~~~~~~~~~~~~~~~~~


The python environment is installed from the miniconda3 distribution (Anaconda).

The shell command interpreter must be bash.

First, add the path of bin directory of the miniconda3 installation on the environment
variable PATH.

**Then, define in an environment variable the version of eo-dataflow-manager:**

.. code-block:: bash

  VERSION=7.8.0

**Create the Conda environment of the eo-dataflow-manager and install eccodes tools:**

.. code-block:: bash

  conda create -y -n eo-dataflow-manager-$VERSION python=3.8 eccodes -c conda-forge


Enable python environment
~~~~~~~~~~~~~~~~~~~~~~~~~


**To enable the python environment specific to eo-dataflow-manager:**

.. code-block:: bash

  conda activate eo-dataflow-manager-$VERSION



Install from gitlab pypi repository
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Install command:**


.. code-block:: bash

  pip install eo-dataflow-manager[rabbitmq]==$VERSION --extra-index-url https://gitlab.ifremer.fr/api/v4/projects/1225/packages/pypi/simple



Files are installed inside the current Python environment.





Deployment of eo-dataflow-manager
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




No deployment is necessary. During its first launch, the eo-dataflow-manager creates its
execution environment, according to the "workspace" and "appdata" parameters of the
configuration file.

Startup eo-dataflow-manager
~~~~~~~~~~~~~~~~~~~~~~~~~~~


**Start command**:

.. code-block:: bash

  usage: eo-dataflow-manager [-h] [-w PATH] [-a PATH]
                             [-c FILE] [-l FILE]
                             [-d] [-v] [-f]

  optional arguments:
    -h, --help            show this help message and exit
    -w PATH, --workspace_dir PATH
                          Set PATH as the workspace directory of the System Controller
    -a PATH, --appdata_dir PATH
                          Set PATH as the application data directory of the System Controller
    -c FILE, --config FILE
                          Set FILE as the global System Controller configuration file
    -l FILE, --log-file FILE
                          Set FILE as the log file of the System Controller
    -d, --daemon          Activate the daemonization of the System Controller
    -v, --verbose         Activate verbose output (show log in STDOUT at DEBUG level)
    -f, --force           Force deletion of the lock file (if it exists)



Stop eo-dataflow-manager
~~~~~~~~~~~~~~~~~~~~~~~~


**Stop command**:

If the eo-dataflow-manager is in interactive mode, just press “ Ctrl-C“.

Otherwise, if the eo-dataflow-manager is in daemonization mode, find the eo-dataflow-
manager’s pid and:

.. code-block:: bash

  kill -TERM <pid>






Uninstall eo-dataflow-manager
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


**Uninstall workspace and configuration command:**


.. code-block:: bash

  rm -rf <workspace_directory> 
  rm -rf <application_data_directory>





**Uninstall package command:**

.. code-block:: bash

  pip uninstall eo-dataflow-manager 



**Remove the Conda environment of the eo-dataflow-manager command:**

.. code-block:: bash

  conda env remove -n eo-dataflow-manager


Installation with docker
------------------------

Prerequisite
~~~~~~~~~~~~


The installation procedure works with the docker environment:

* ubuntu 22.04 / debian 10.13

* docker 24.0.5


Get the docker image
~~~~~~~~~~~~~~~~~~~~

the docker image is available on the Ifremer gitlab repository

**First, define in an environment variable the version of eo-dataflow-manager:**

.. code-block:: bash

  VERSION=7.9.0


** pull docker command:**

.. code-block:: bash

  docker pull gitlab-registry.ifremer.fr/eo-dataflow/eo-dataflow-manager:$VERSION


Create and run a new container from an image
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You must first list the "directories or file systems" available on the host machine you want to access. This includes: input directories, storage directories and the application configuration directory as well as directories containing download configuration files and workspace  (those defined in the application configuration file).
For each of them, in the common 'docker run' below, you will have to add the option "--volume directory_path:directory_path"

**First, define in an environment variable the version of eo-dataflow-manager:**

.. code-block:: bash

  VERSION=7.9.0



** command to launch eo-dataflow-manager:**

.. code-block:: bash


  docker run -v $DATA:$DATA --user $USERID:GROUPID --network=host --name eo-dataflow-manager gitlab-registry.ifremer.fr/eo-dataflow/eo-dataflow-manager:$VERSION eo-dataflow-manager -c /data/eo_dataflow/eo-dataflow-conf.yaml [--appdata_dir $APPDIR --workspace_dir $WORKSPACE]


