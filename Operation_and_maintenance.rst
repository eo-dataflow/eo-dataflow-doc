.. -*- coding: utf-8 -*-

Operation and maintenance
=========================


Database user guides
--------------------


This section describes the schema of the databases used by the **eo-dataflow-manager**
tools. Each configured download uses two databases:


* a database keeping an inventory of the available files on the source (remote) repository
  and their properties (referred to as the **dcheck database**)

* a database of the files actually downloaded from the source repository (referred to as
  **download database**)



All databases are sql files. The databases for a configured download (with identifier
<download id>) are located in:




+-------------------------------------------------+
| $workspace_dir/downloads/<download id>/internal |
+-------------------------------------------------+





Dcheck database
~~~~~~~~~~~~~~~


Each configured download has a **dcheck_database.db** file. This file gives the files
found on the remote server. Information is organized in four tables:


1. *executions*

#. *files*

#. *constants_infos*

#. *exec_infos*

It’s use to generate the listings of files to download.

*Table executions*

This table records each dcheck execution.




+-----------------+-----------------+-----------------------------------------------+
|      Column     |       Type      |                    Comment                    |
+=================+=================+===============================================+
| id              | INTEGER         | Primary key                                   |
+-----------------+-----------------+-----------------------------------------------+
| date_start      | TIMESTAMP       | When dcheck started                           |
+-----------------+-----------------+-----------------------------------------------+
| date_stop       | TIMESTAMP       | When dcheck finished                          |
+-----------------+-----------------+-----------------------------------------------+
| valid_execution | BOOLEAN (0,1)   | Indicates if the dcheck encountered errors    |
+-----------------+-----------------+-----------------------------------------------+



*Table files*

This table records all the file paths scanned during a dcheck.




+--------------+---------------+--------------------------------------+
|    Column    |      Type     |               Comment                |
+==============+===============+======================================+
| id_execution | INTEGER       | Foreign key references executions.id |
+--------------+---------------+--------------------------------------+
| filename     | VARCHAR       | Path of the scanned file             |
+--------------+---------------+--------------------------------------+
| isDirectory  | BOOLEAN (0,1) | Is this file a directory ?           |
+--------------+---------------+--------------------------------------+
| isSymlink    | BOOLEAN (0,1) | Is this file a symbolic link ?       |
+--------------+---------------+--------------------------------------+
| size         | INTEGER       | Size of the file in bytes            |
+--------------+---------------+--------------------------------------+
| mtime        | TIMESTAMP     | Modification time of the file        |
+--------------+---------------+--------------------------------------+
| sensingtime  | TIMESTAMP     | Acquiring time of the data           |
+--------------+---------------+--------------------------------------+



*Table constants_infos*

This table records metadata of each dcheck execution. Mainly, it’s indicates the current
configuration as a xml tree. Moreover, it can add specific data for each protocol.

For instance, when the dcheck scans a FTP, it’s add some informations like ftp file system
type, ftp welcome text.




+--------------+--------------+--------------------------------------+
|    Column    |     Type     |               Comment                |
+==============+==============+======================================+
| id           | INTEGER      | Primary key                          |
+--------------+--------------+--------------------------------------+
| id_execution | INTEGER      | Foreign key references executions.id |
+--------------+--------------+--------------------------------------+
| title        | VARCHAR      | name of the constant                 |
+--------------+--------------+--------------------------------------+
| data         | TEXT         | related text (xml, log, …)           |
+--------------+--------------+--------------------------------------+



*Table exec_infos*

This table records mainly the errors occurred during a dcheck.




+--------------+--------------+--------------------------------------+
|    Column    |     Type     |               Comment                |
+==============+==============+======================================+
| id           | INTEGER      | Primary key                          |
+--------------+--------------+--------------------------------------+
| id_execution | INTEGER      | Foreign key references executions.id |
+--------------+--------------+--------------------------------------+
| date         | TIMESTAMP    | Date and time of the error           |
+--------------+--------------+--------------------------------------+
| title        | VARCHAR      | Class name of the error              |
+--------------+--------------+--------------------------------------+
| data         | TEXT         | Error argument values                |
+--------------+--------------+--------------------------------------+



Download database
~~~~~~~~~~~~~~~~~


Additionally, each download has also a **downloader_<id_download>.db** database file (in
the same directory than the previous database). This database contains a unique table
named download_<id_download> which list the files that will be downloaded and it’s updated
when a new listing is generated.



*Table download_<id_download>*


+------------------------+-------------------------+---------------------------------------------------------------------+
|         Column         |           Type          |                               Comment                               |
+========================+=========================+=====================================================================+
| id                     | INTEGER                 | Primary key                                                         |
+------------------------+-------------------------+---------------------------------------------------------------------+
| filepath               | VARCHAR                 | The remote path of the file to download                             |
+------------------------+-------------------------+---------------------------------------------------------------------+
| state                  | INTEGER                 | 0: not downloaded, 1: error, 2: successful download, 3: re-download |
|                        |                         | or -1, -2, -3, -4 in the case of retry before error                 |
+------------------------+-------------------------+---------------------------------------------------------------------+
| ignition_date          | TIMESTAMP               | insertion date in database of the file to download                  |
+------------------------+-------------------------+---------------------------------------------------------------------+
| last_update            | TIMESTAMP               | Last update of the remote file                                      |
+------------------------+-------------------------+---------------------------------------------------------------------+
| size                   | INTEGER                 | size of the file                                                    |
+------------------------+-------------------------+---------------------------------------------------------------------+
| mtime                  | TIMESTAMP               | modification time of the file                                       |
+------------------------+-------------------------+---------------------------------------------------------------------+
| sensingtime            | TIMESTAMP               | Acquiring time of the data                                          |
+------------------------+-------------------------+---------------------------------------------------------------------+



Developer guides
----------------


Development facility
~~~~~~~~~~~~~~~~~~~~

Installing conda environment
++++++++++++++++++++++++++++

**Create the Conda environment of the eo-dataflow-manager and install eccodes tools:**

.. code-block:: bash

  conda create -y -n eo-dataflow-manager python=3.8 poetry=1.3.1 eccodes -c conda-forge
  conda activate eo-dataflow-manager


Move to the parent directory for the eo-dataflow-manager sources.


**Install the sources by cloning from the gitlab repository:**

.. code-block:: bash

  git clone https://gitlab.ifremer.fr/eo-dataflow/eo-dataflow-manager.git



Install / upgrade eo-dataflow-manager
+++++++++++++++++++++++++++++++++++++

**Install command:**

.. code-block:: bash

  poetry install --all-extras




Files are installed inside the current Python environment.





Map a remote path to a local path
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Define a new DataReader
+++++++++++++++++++++++


If your download has specific rules to defines the output path of your downloaded files,
you have to create a class in eo-dataflow-manager/scheduler/plugins/DataReader/ and
inheriting IDataReader.



You have to implement the following methods:


* **getRelativePath(self, filepath)**: map the remote filepath to a local filepath relative
  to the local storage directory.

* **getStorageName(self, filepath)**: map the remote filepath to a local filename.

* **getDate(self, filepath)**: *see next section*.



Example

.. code-block:: python

  from eo-dataflow-manager.scheduler.sc.IDataReader import IDataReader
  import logging, datetime, os

  class BasicDataReader(IDataReader):
      def __init__(self,logger,date_regexp=None,date_fmt=None,storage_path=None):
          self._log = logging.getLogger(logger)

      def getRelativePath(self, filepath):
          # on supprime les '/' du filepath... sinon c'est le permission denied 
          # assure pour le stockage local...
          filepath = filepath.lstrip('/') 
          return filepath 
      
      def getStorageName(self, filepath):
          filedir, filename = os.path.split(filepath)
          return** filename

      def getDate(self, filepath):
          return** None


Actually, existing implementations of the DataReader are:


* **RegexpDataReader**: this DataReader use a regular expression to extract the date from
  the remote file name and the output path can be configured by a specific date format
  expression.

* **DirRegexpDataReader**: this one uses a regular expression to retrieve the date from the
  remote directory name and the output path can be configured by a specific date format
  expression.

* **NetCDFDataReader**: this other uses the name of a NetCDF global attribute to retrieve
  the date. In the same way, the output path can be configured by a specific date format
  expression.

However, you can define your own data reader and put it in the same directory.

You can define a specific constructor if your data reader needs some parameters.

Update the DataReaderFactory
++++++++++++++++++++++++++++


The method DataReaderFactory(...) is a method which parameters are the union of all the
parameters of each DataReader. The first parameter is the classname of the DataReader. So,
this method has to instantiate the correct class with the correct constructor.



This method uses a cache (know_classes) to store all python modules corresponding to the
classnames given in parameters. Thus, it instantiate the correct DataReader like this:
know_classes[classname](param1, param2, …).



Actually there is two python files where the method DataReaderFactory is defined:


* eo-dataflow-manager/scheduler/plugins/Factory.py

* eo-dataflow-manager/dchecktools/plugins/DataReaderFactory.py

You need to update both.

Update the xml reader
+++++++++++++++++++++


If you want to add specific parameters to your data reader, you have also to update the
file eo-dataflow-manager/scheduler/sc/ConfigurationFileUtil.py, especially the
read() method of ConfigurationSource class which extract the data reader parameters from the xml tags of the
file xxx_download.xml. Don’t avoid to add your additional variables in the returned list.



.. code-block:: python

    class ConfigurationSource(object):
        ...
        def read(self):
            result = True
            ...
            if self.__xr.haveSubNode(self.__node, 'date_extraction'):
                date_extraction_node = self.__xr.getSubNode(
                    logging.ERROR, self.__node, 'date_extraction')
                if 'plugin' in date_extraction_node.attrib:
                    self.__plugin = self.__xr.getAttributeValue(
                        logging.WARNING, date_extraction_node, 'plugin')
                else:
                    self._log.error(
                        "the node '%s' don't have the required attribute '%s'" %
                        (date_extraction_node.tag, 'plugin'))
                    result = False

                if self.__xr.haveSubNode(date_extraction_node, 'regexp'):
                    self.__dateRegexp = self.__xr.getSubNodeValue(
                        logging.ERROR, date_extraction_node, 'regexp')
                else:
                    self._log.error(
                        "the node '%s' don't have the required node '%s'" %
                        (date_extraction_node.tag, 'regexp'))
                    result = False

                if self.__xr.haveSubNode(date_extraction_node, 'format'):
                    self.__dateFormat = self.__xr.getSubNodeValue(
                        logging.ERROR, date_extraction_node, 'format')
                else:
                    self._log.error(
                        "the node '%s' don't have the required node '%s'" %
                        (date_extraction_node.tag, 'format'))
                    result = False



Then, edit the file eo-dataflow-manager/synchronisation/SynchronisationLocalRemote.py, and
update the method read() which calls ConfigurationFileUtil.ConfigurationFileUtil.Read(...). Again, add
variables in the left expression, if needed, to store the additional returned variables.
Finally, don’t avoid to pass these values to Factory.DataReaderFactory(...) called in the
next lines.

Update the dcheck tool
++++++++++++++++++++++


You may have to update also the dcheck tool, located in eo-dataflow-manager/dchecktools/dcheck.py
and ensures that its method run() provides the required
parameters to the method DataReaderFactory(...) called.



Check the parameters you give in xxx_download.xml in the <dcheck_config> are taken into
account in the getNodeString(self, nodename) method.



Check also the constConfigString() method, which generate a xml string representing the
dcheck configuration for the download, contain a xml node for each parameter.

Reference the data reader in the concerned downloads
++++++++++++++++++++++++++++++++++++++++++++++++++++


Obviously, you have to configure your download with the new data reader. This is done in
the xxx_download.xml (see “configuration” section, “download configuration” subsection).
Specify the *plugin_name* corresponding to the DataReader class name and specify the
required parameters as children xml tags.



**Example**

.. code-block:: html

  <download_config id="myDownload"> 
    ... 
      <data_reader plugin_name="MyDataReader">  
        <param1>.*_([0-9]{8})_.*</param1> 
        <param2>%Y%m%d</param2>
        <param3></param3>
      </data_reader>
    ...
  </download_config>


Compute the date from a remote url
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


If you want to define a new way to extract the date from a remote file, *e.g. if date is
contained in a parent directory name, filename contains multiple dates or in a exotic
format*, you may have to define a specific DataReader or to update one existing.



Check the previous section about DataReader development. For this purpose, you have to
implement the method getDate(filepath) which have to return a datetime object from a
remote file path.

Notes that you have to define together the way you extract the date and the way to map the
remote path to the local path. They cannot be separated except if the DataReader delegates
one of them to another parameter object.

Support a protocol for scanning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Define a new AbstractProtocol
+++++++++++++++++++++++++++++


In order to support a new protocol or an existing one used in a specific context, you can
create a class in eo-dataflow-manager/dchecktools/protocols/ and inheriting
AbstractProtocol.



You have to implement the following methods:


* **getFileInfoList(self)**: crawl on the main remote directory (self.path) all interesting
  files (including subdirectories) and add each one in the database:
  self.fileInfoDatabase.addFile(localfile).

* **setDirectoryFilter(self, directoryFilter)**: set the current directory filter. The
  getFileInfoList() method must consider it.

* **setFileFilter(self, filefilter)**: set the current file filter. The getFileInfoList()
  method must consider it.

** see ../filters/StringFilters.py*



Example

.. code-block:: python

  from eo_dataflow_manager.dchecktools.protocols.AbstractProtocol import AbstractProtocol
  from eo_dataflow_manager.dchecktools.common.basefileinfos import File
  from eo_dataflow_manager.dchecktools.common.errors import DC_ConfigError, DC_LocalpathError

  import logging, sys, os, stat, time
  log = logging.getLogger('localpath')
  log.setLevel(logging.INFO)
 
  class Protocol_localpath(AbstractProtocol):
      def __init__(self):
          AbstractProtocol.__init__(self)
          self.check_infos = [ 'size', 'mtime', 'sensingtime' ]
          
      def setConfig(self, config):
          AbstractProtocol.setConfig(self, config)

      def setDirectoryFilter(self, directoryFilter):
          self.directoryFilter = directoryFilter
          
      def setFileFilter(self, fileFilter):
          self.fileFilter = fileFilter

      def getFileInfoList(self):
          if os.path.exists(self.path):
              os.path.walk(self.path, self.__walk_hook, None)
          elif len(self.directories_smart_crawler) == 0:
              error = DC_LocalpathError("path does not exists : %s"%(self.path))
              log.error(error)
              sys.exit(1)
          else:
              log.warning("path does not exists : %s"%(self.path))
          self.updateValidExecutionStatus(True)

      def __walk_hook(self, arg, dirname, fnames):
          current_directory_interesting = True
          if self.directoryFilter != None:
              if not self.directoryFilter.isInteresting(dirname):
                  current_directory_interesting = False
          for fname in fnames:
              self.nbr_walked_files += 1
              current_path = os.path.join(dirname, fname)
              islink = os.path.islink(current_path)
              isdirectory = os.path.isdir(current_path)
              if not current_directory_interesting and not isdirectory:
                  continue
              if self.fileFilter != None:
                  interestingFile = self.fileFilter.isInteresting(current_path)
                  if not interestingFile:
                      continue

              localfile = File()
              localfile.filename = current_path
              localfile.isDirectory = isdirectory
              localfile.isSymLink = islink

              if 'size' in self.check_infos:
                  localfile.size = file_stat[stat.ST_SIZE]
                  
              # ...
              
              date, insert = self.getSensingTime(localfile.filename)
              if date != None:
                  localfile.sensingtime = date
              if file != None and insert:
                  self.fileInfoDatabase.addFile(localfile)




Actually, existing protocols are:


* **Protocol_localpath** (localpath.py): files are retrieved from a repository available in
  the local file system.

* **Protocol_ftp** (ftp.py): files are retrieved from a ftp.

* … (see Download configuration section).

Update the dcheck tool
++++++++++++++++++++++


When you have defined your protocol, edit the file eo-dataflow-
manager/dchecktools/dcheck.py and update the method getProtocolObject(self, protocolname)
in order to instantiate your protocol object according to **an associated name you have to
define**.



**Example**

.. code-block:: python

  def getProtocolObject(self, protocolname):
    obj = None
    if protocolname == "localpath":
      obj= localpath.Protocol_localpath()
    elif** protocolname == "ftp":
      obj = ftp.Protocol_ftp()
    return obj

Specify when your protocol must be used
+++++++++++++++++++++++++++++++++++++++


Edit the file eo-dataflow-manager/dchecktools/common/spliturl.py.

Update the method getProtocolName() which returns a protocol name associated to an url.
So, if you have a specific way to extract links from a specific http page, you may have to
define your own protocol and to specify that it must be used for a given domain (like the
following example).



**Example**

.. code-block:: python

  def getProtocolName(scheme, path, domain, exitonfail=True):
      if scheme == 'ftp':
          return 'ftp
      elif scheme == 'http':
          return 'http'
      elif scheme == 'https': 
          if (domain == "oceandata.sci.gsfc.nasa.gov"): 
              return "http_filetable"
          else 
              return "http_directorylist"
      elif scheme == 'file' \
              or (scheme == '' **and** path.startswith('/')) \
              or (scheme == '' **and** path.startswith('./')):
          return 'localpath'
      elif scheme == 'lslr': 
          return 'lslr_file'
      elif scheme == 'https_opensearch': 
          return 'https_opensearch'
      else
          if exitonfail:
              error = DC_ConfigError("unknown protocol for url : %s"%(path)) 
              log.error(error)
              sys.exit(1)
          return None 
      
  def setConfigFromUrl(config, url):
      scheme, domain, path, port, username, password = spliturl(url)
      config.path = path
      config.protocol = getProtocolName(scheme, config.path, domain) 
      ...




Support a protocol for downloading
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Define a new AbstractProvider
+++++++++++++++++++++++++++++


To define a new file transfer method, you have to create class in eo-dataflow-
manager/scheduler/sc/ProviderManager/ inheriting AbstractProvider.



You have to implement the following methods:


* **getFile(self, session, remotepath, localpath)**: download a remote file.

* **putFile(self, session, localpath, remotepath)**: upload a local file.

* **getSession(self, timeout_)**: create a session to the remote url, with a given timeout.

* **closeSession(self, session)**: close the current session.

* **setSpecificState(self)**: set the configuration dictionary.

* **checkAvailability(self):** returns true if a session can be opened.

You must define a constructor with three parameters:


* **id**: the id of the provider

* **type** : the name of the protocol (e.g. “ftp”, “localpath”)

* **job_path**: path where the provider status files are stored



**Example**

.. code-block:: python

  class FtpProvider(AbstractProvider):     

      def __init__(self, id, type, job_path): 
          AbstractProvider.__init__(self, id, type, job_path) 
          self.__username = None
          self.__password = None
          self.__port = DEFAULT_CONNECTION_PORT
          self.__session = None

      def setSpecificState(self): 
          self._jobstate['Server'] = self.__server
          self._jobstate['Username'] = self.__username 
          self._jobstate['Password'] = self.__password 
          self._jobstate['Port'] = self.__port 

      def checkAvailability(self): 
          ret = None 
          self.__session = self.getSession() 
          if self.__session == None: 
              return False
          try:
              ret = self.__session.getwelcome() 
          except socket.error, msg:
              self._log.warning("Connection error : [socket.error : %s]"%(msg))
              ret = False
          try:
              self.__closeSession()
          except Exception, e:
              pass
          if ret != None: 
              return True 

      return False 

      def getSession(self, timeout_=None): 
          session = None 
          try:
              session = ftplib.FTP(self.__server, self.__username, 
                                   self.__password, timeout=timeout_)
          except socket.error, msg:
              self._log.info(
                  "Cannot connect to server [socket.error : %s, (server=%s, username=%s])" %
                  %msg, self.__server, self.__username))
          return session 

      def closeSession(self, session): 
          if session == None:
              return
          try:
              session.close()
          except socket.error, msg:
              self._log.warning("Connection error : [socket.error : %s]"%(msg))
              
      def getFile(self, session, remotepath, localpath):
          tmp_localpath = localpath+".tmp"
          try:   
              session.retrbinary("RETR "+remotepath, open(tmp_localpath, 'wb').write)
          except Exception, msg:
              self._log.warning(
                  "Connection : Exception while retrieving binary : %s (file = %s)" %
                  (msg, remotepath))
              return None 
          try:
              os.rename(tmp_localpath, localpath)
          except OSError, msg: 
              self._log.warning(
                  "Cannot rename %s --> %s (%s)" %
                  (tmp_localpath, localpath, msg))
              return None 
          if os.path.exists(localpath): 
              return File(localpath) 
          return None  

      ...


Note that methods can raise RuntimeError if they are not implemented.

Add provider parameters (outside the constructor)
+++++++++++++++++++++++++++++++++++++++++++++++++


If you want to define additional setters on your class, you can edit the file
ProviderManager.py in the same directory, in particular the read(...) method like this:



Example

.. code-block:: python

  class ProviderManager: 

      ...

      def read(self, hostConfig, execPath=None):
          ...
          provider = ProviderFactory(provider_type, provider_id, self.__job_path)
          if provider_type == "Ftp":
              server = xr.getSubNodeValue(logging.ERROR, tmpNode, 'server')
              username = xr.getSubNodeValue(logging.ERROR, tmpNode, 'username')
              password = xr.getSubNodeValue(logging.ERROR, tmpNode, 'password')
              port = xr.getSubNodeValue(logging.ERROR, tmpNode, 'port', "")
              if port != "":
                  port = int(port)
              else:
                  port = None 

              provider.setServer(server)
              provider.setUsername(username)
              provider.setPassword(password)
              rovider.setPort(port)
          ...


This method read the download configuration xml file and extract, for each protocol, the
required parameters. So, if you have additional parameters, you can modify this method for
your specific protocol and add these parameters in the configuration file.

Define how the remote url is computed with the parameters
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++


If you add a protocol you must define how the url is computed from the standard parameters
(like server, username, password, port, repository) and, optionally, from the additional
parameters you defined in the previous section.



To do so, edit the file DCheckReportListingBuilder.py located in eo-dataflow-
manager/scheduler/plugins/ListingBuilder/. You have to modify the checkProvider(self)
method: you must specify how the __base_url is computed for the given protocol and
parameters.



**Example**

.. code-block:: python

  def checkProvider(self):
      provider = self.__download.provider 
      provider_type = provider.__class__.__name__
      self._log.debug(" provider_type '%s'" % ( provider_type))
      if provider_type == 'FtpProvider':
          self.__base_url = 'ftp://'
          if provider.username != None: 
              self.__base_url += provider.username 
          if provider.password != None:
              self.__base_url += ':'+provider.password
          if provider.username != None or provider.password != None:
              self.__base_url += '@' 
              elf.__base_url += '%s'%(provider.server)
          if provider.port != None:
              self.__base_url += ':%s'%(str(provider.port)) 
              self.__base_url += '/'+self.__download.remote_storage_repository
          elif provider_type in ['LocalpathProvider', 'LocalpointerProvider', 
                                 'OnlyNotifyProvider','LocalmoveProvider']:
              self.__base_url = self.__download.remote_storage_repository 
          else:
              raise Exception(
                  'DCheckReportListingBuilder : unknown provider : %s. Plugin is disabled !' %
                  (provider_type))
              self.__activated = False


Operation and maintenance tools
-------------------------------


This section describes the common operation and maintenance commands.


Cleaning eo-dataflow-manager workspaces
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The more efficient way to free space on the eo-dataflow-manager is to delete voluminous
dcheck databases.

When you have identified the databases to delete with the command:

.. code-block:: bash

  ls -l $MyWorkspace/downloads/*/internal/dcheck_database.db -h | grep G
  
  
then do as follow:


1. Disable the identified download (add .OFF extension to its configuration file),

#. Rename the dcheck_database.db file to dcheck_database.db.old,

#. Reactivate the identified download (remove the .OFF extension to its configuration file),

#. Creating a new dcheck_database.db file will be done automatically with the launch of the
   first dcheck and the first scan,

#. Open the database with sqlite3 dcheck_database.db.old,

#. Redirect the output of sqlite3 (.output exe_old.list),

#. Make a selection request on the files of the last analysis with:

   select count(*) from files where id_execution=(select max(id) from executions) and
   date(mtime)<=date('now','-1day') order by filename;

#. Close this database (.q),

#. Once the first scan is done (end of the dcheck), open the database with sqlite3
   dcheck_database.db,

#. Redirect the output of sqlite3 (.output exe_new.list),

#. Make a selection request on the files of the last analysis with:

   select count(*) from files where id_execution = 1 and date(mtime) <= date('now',’-1day')
   order by filename;

#. Close this database (.q),

#. Compare the two lists to identify new files:

diff exe_old.list exe_new.list > new_files.list


1. Drop the new_files.list file into the "orders/listings/manual" directory.

#. Delete the dcheck_database.db.old file,

Scanning data (using dcheck command)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


This tool check files from a data provider url and fill a database used by the eo-
dataflow-manager.

After two successives checks, the eo-dataflow-manager compares their files and determines
which are news or modified and, thus, need to be downloaded.



The dcheck tool loop on files from the provider url (ftp, local directory, http...) and
determines if it's interesting on them. If so, it add the file in the database. To do
that, it requires a configuration file «$MyAppdata/download/xxx_download.xml ».



*Found in $MyCondaEnv/bin* **/** *dchecktools*



.. code-block:: bash

  eo-dataflow-manager-dcheck <provider_url>
                             -c|--config <download_configuration_file>
                             [-d|--database <dcheck_database>]
                             [--purge-older-than <days>] 
                             [--purge-remove-last] 
                             [--smart-crawler]
                             [--follow_symlink]
                             [--ftp-listing-type <unix|unix2>]
                             [--debug]
                             [-t|--test]                               |
                             [--help] 
                             [--version] 




**-d, --database <dcheck_database>** Specify the path of the remote provider data sqlite
database in which current scan data will be added. Each download has its own database
files (one for remote provider data, another for local data).

**--purge-older-than <days>** The purge deletes executions (table executions) earlier to a
given date. Related files are also deleted from the database, regardless of their own
modification date.

The purge is automatically and exclusively executed inside of the dcheck command, for
concurrency access reasons and is applied by default to the executions earlier than the
last 60 days.

**--purge-remove-last** By default, the purge keeps the last execution even if it’s
outdated. This option forces the purge of the last execution if needed.

**--smart-crawler** Enable smart crawler options (see below).

**--follow_symlink** If specified, follow symlink for directories.

**--ftp-listing-type** Specify listing type provided by the given url. Actually, this
option is only used for FTP providers and its default value is “unix” if not specified.

**-t, --test** Don’t add files to database, just display.


**Example**

.. code-block:: bash

  ssh cerexp@vcerdownload1-test

  cd /export/home/python/environments/eo-dataflow-manager

  source bin/activate /export/home/python/environments/eo-dataflow-manager
  eo-dataflow-manager-dcheck ftp://anonymous:anonymous@ifremer.fr@ftp.star.nesdis.noaa.gov/pub/sod/lsa/cs2igdr -c /export/home/python/applications/eo-dataflow-manager_v7/appdata/downloads/noaa_cryosat2_igdr2.download.xml -d /tmp/test.db --debug




Synchronize the inventory database (downloadsync)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




The downloadsync command is used when the local inventory and download databases of a
download ( **dcheck** and **download** databases) are out of sync with the target (local)
repository of downloaded files or the source (remote) repository. This can happen for
instance if files have been manually downloaded or removed without updating the database.



This tool generates a listing of the files to download in sync with the source (remote)
repository. This listing can be automatically moved to the “auto” directory to order to
download the specified files.



By default, all files of the last execution are considered but it’s possible to constrain
lower date, upper date or both via the --min-date and --max-date options described below.



This tool works as follow:


1. dcheck scans the files in the local storage (paths stored in < local_database_path » or by
   default in « synch_local_database.db » in the "internal" directory of the download):

  * URL: "local_url" (default <data_destination><location> of configuration file xxx_download.xml)

  * Retrieve a list containing all the files from the last run (local_list).



2. A dcheck scans remote data (paths stored in « remote database_pah » or by default in
   « synch_lremote_database.db » in the "internal" directory of the download) :

   * URL: generated from the provider type and <data_source><location> information from the
     xxx_download.xml provider.



  * Retrieve a list containing all files of the last execution (remote_list).




3. Loop on each file from the remote_list to generate the listing:


  * The current file is added to the list if the local list does not contain it,

  * If the current file is in the local list, if the redownload option is active and if the
    mtime of the remote file is greater than the mtime of the local file then the current file
    is added to the list.





Difference with the eo-dataflow-manager: in addition to manage downloads lifecycle, the
eo-dataflow-manager differ from the eo-dataflow-manager-sync tool:


1. A dcheck scans the remote data

#. A dreport scans the local dcheck_database

The objective of the eo-dataflow-manager is to feed continuously the listings whereas the
eo-dataflow-manager-sync is used where local downloaded files are desynchronized with the
local database.



*Found in $MyCondaEnv/bin*



.. code-block:: bash

  eo-dataflow-manager-sync -w|--workspace_dir <workspace_path> 
                           -a|--appdata_dir <applicationdata_path> 
                           -c|--config <download_configuration_file>
                           [-l|--local <local_database_path>]
                           [-r|--remote <remote_database_path>]
                           [-o|--output <output_listing_file>]
                           [-u|--localpath <local_url>]
                           [--with-download]
                           [--redownload] 
                           [--min-date <date>] 
                           [--max-date <date>] 
                           [--history-path <history_jobstate_path>]
                           [-j|--job-path <jobstate_path>"]
                           [-v|--verbose]
                           [-h|--help ] 



**Options**



**--with-download** By default, eo-dataflow-manager-sync only generates the listing. With this
option, it will move it automatically to the «auto» directory.



**-u, --localpath <local_url>** Specify where files are archived in the case of spool
repositories.



*eo-dataflow-manager-sync owns an option to include modified files in the listing.
However, this option is not currently available from the command line (disabled).*



 **Example**

.. code-block:: bash

  ssh cerexp@vcerdownload1-test
  
  cd /export/home/python/environments/eo-dataflow-manager
  source bin/activate /export/home/python/environments/eo-dataflow-manager
  eo-dataflow-manager-downloadsync -w /export/home/python/applications/eo-dataflow-manager_v7/Workspace –a /export/home/python/applications/eo-dataflow-manager/Appdata -c /tmp/myDownloadConfFile_download.xml -o /tmp/myDownload.list -v
  
  # To use the listing and download the missing files add --with-download



Reloading files whose state is 'Error' (error-files-reloader)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The reloader command is used to download files again. All files with a status of '1' are selected.

From this selection of files, this tool updates the download base by updating the "state" field to 3 (redownload).


*Found in $MyCondaEnv/bin*


.. code-block:: bash

 eo-dataflow-manager-error-files-reloader -w|--workspace_dir <workspace_path>
                                          -a|--appdata_dir <applicationdata_path>
                                          -c|--config <download_configuration_file>
                                          [-v|--verbose]
                                          [-h|--help ]


**exemple:**



.. code-block:: bash

  ssh cerexp@vcerdownload1-test

  cd /export/home/python/environments/eo-dataflow-manager_v7

  source bin/activate /export/home/python/environments/eo-dataflow-manager_v7 
  eo-dataflow-manager-error-files-reloader -w /export/home/python/applications/eo-dataflow-manager_v7/Workspace –a /export/home/python/applications/eo-dataflow-manager_v7/Appdata -c /tmp/myDownloadConfFile_download.xml


Reloading files from a list contained in a file (reloader)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The reloader command is used to download files again. The list of files to reload is
contained in a file “input_list_file”.

From the list of files, this tool updates the download database by updating the "state"
field to 3 (redownload).



This tool works as follow:


  For each line contained in the input file, the tool selects, in the database, the file
  names corresponding to the string of the line. The characters "%" and "_" in the string
  are interpreted as wildcards.

  For "SAFE" downloads, the string must indicate the "SAFE" directory (reloading is
  performed on all files in the "SAFE" directory).

  For archive downloads (.zip with multiple files or .tar), the string must indicate the
  original file (.tar or .zip).





*Found in $MyCondaEnv/bin*


.. code-block:: bash

 eo-dataflow-manager-reloader -w|--workspace_dir <workspace_path>
                              -a|--appdata_dir <applicationdata_path>
                              -c|--config <download_configuration_file>
                              -i|--input <input_list_file>
                              [-v|--verbose]
                              [-h|--help ]


*Options**


**-i|--input <input_list_file>**  Specify filepath which contains the list of files to reload.


**exemple:**



.. code-block:: bash

  ssh cerexp@vcerdownload1-test

  cd /export/home/python/environments/eo-dataflow-manager_v7

  source bin/activate /export/home/python/environments/eo-dataflow-manager_v7
  eo-dataflow-manager-reloader -w /export/home/python/applications/eo-dataflow-manager_v7/Workspace –a /export/home/python/applications/eo-dataflow-manager_v7/Appdata -c /tmp/myDownloadConfFile_download.xml -i /tmp/myList.list


Monitoring and troubleshooting
------------------------------


This section describes how to monitor the internal behaviour of the **eo-dataflow-
manager** or to investigate download issues.



Check the logs
~~~~~~~~~~~~~~


The main sources for log messages are the following files:


* the main scheduler log file :


+--------------------------------+
| $MyWorkspace/log/scheduler.log |
+--------------------------------+




* the log file restricted to a specific download :


+---------------------------------------------+
| $MyWorkspace/log/download_<id_download>.log |
+---------------------------------------------+



These files can indicates the current state of a download or eventual exceptions.



Check the download workspace
~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The workspace of a specific download is located in:




+------------------------------------------+
| $MyWorkspace/downloads/*<id_download>*/  |
+------------------------------------------+



It contains in particular the current listings of files to download in:




+--------------------------------------------------------------+
| $MyWorkspace/downloads/*<id_download>*/orders/listings/auto  |
+--------------------------------------------------------------+



Listings currently being processed have a *.lock* extension. **Old locked files still
present in this directory are an indication of stuck downloads**. This is usually caused
by a listing too long (longer than the to_download_max_nbr limit specified in the download
configuration file) : it is a protection against a massive download that would preempt all
the ressource.



A way to enforce these pending download is for instance to split the locked file in
smaller chunks (lower than the maximum limit set for the download), for instance using the
*split* linux command:



.. code-block:: bash

  split -dl <to_download_max_nbr> <mylisting.list> --additional-suffix=.list 






The archive of the previously completed listings are in:




+-----------------------------------------------------------+
| $MyWorkspace/downloads/ *<id_download>*/internal/listings |
+-----------------------------------------------------------+





The temporary buffer where files are actually downloaded is :




+---------------------------------------------------------+
| $MyWorkspace/downloads/ *<id_download>*/data/temporary/ |
+---------------------------------------------------------+



Files and folders (ZIP unzip, TAR extract or full SAFE) that could not be copied to their
final target directory after download are located in :




+-------------------------------------------------------+
| $MyWorkspace/downloads/ *<id_download>*/data/to_store |
+-------------------------------------------------------+



The SAFE directories before being completely downloaded are located in :




+---------------------------------------------------+
| $MyWorkspace/downloads/ *<id_download>*/data/safe |
+---------------------------------------------------+



File links that could not be copied to their final target directory after they were
created are located in:



+---------------------------------------------------+
| $MyWorkspace/downloads/ *<id_download>*/data/link |
+---------------------------------------------------+



Check the current jobs of the eo-dataflow-manager
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The current jobs are indicated in **$MyWorkspace/cache/jobfile/**. Old dates indicates
probably that errors occurred or download freezes.



The current connections are indicated in **$MyWorkspace/cache/providerfile/**. You can
check here if your download is currently running if the connection to the server is
present in this directory.



Check other messages
~~~~~~~~~~~~~~~~~~~~


Other messages can be found in **$MyWorkspace/spools/message/** or in **$MyWorkspace/spools/message/**.
This directory can contain many .xml files or .json files. You can use the grep command to find informations
about your download.



Check the download databases
~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Database files are located in **$MyWorkspace/downloads/<id_download>/internal/**



*Dcheck database*

Each download owns a reference database used to create the listings of files to download.

Such a reference database is located in: **dcheck_database.db**.

You can check:


* table executions: take the last execution id. You have also the start datetime and
  eventually the stop datetime of the execution if it is finished.

* table files: filter the files by the execution_id. Are you expecting such files ?



*Download database*

Each download owns also it’s proper database listing only the files to download.

Such a database is located in: **downloader_<id_download>.db**.



This database contains only one table. You can check the contents of the "state" column
of each file:


* 0 : not downloaded

* 1 : error

* 2 : downloaded successfully

* 3 : to redownload

* -1, -2, -3, -4 : -n download errors, try again



Check the scan
~~~~~~~~~~~~~~


Finally, you can use the dcheck command yourself, on the command line (described
previously), to monitor the analysis of the remote repository: it indicates whether each
directory is accessible or access denied and lists the files in the directory. To do this,
use the "--debug" option.



